# Employee-Demo

Employee-Demo adalah contoh project pengaplikasian _**CRUD (Create, Read, Update, Delete)**_ API Employee berdasarkan Skema entity relationship database pada https://dev.mysql.com/doc/employee/en/sakila-structure.html menggunakan Java dan Springboot dengan desain MVC pattern.

## Requirements

1. Springboot 2.6.0
2. OpenJDK 11 Zulu Build (https://www.azul.com/downloads/?package=jdk)
3. MariaDB 10.5.12
4. H2 DB for unit and integration test
5. REST API
6. Swagger UI untuk API Documentation
7. Maven 3.6.3
8. Spring Data JPA
9. Hibernate
10. JUnit 5 jupiter
11. Mockito

## Setup dan Build
Langkah-langkah untuk Setup dan Build:

1. Setup `application.properties` sesuaikan dengan settingan MariaDB yang telah terinstall.
2. Jalankan pada console atau command prompt atau bisa melalui IDE seperti Intellij IDEA ```$.\employee-demo>mvn clean install```. Pastikan sebelumnya sudah terinstall Maven 3 dan JDK 11.
3. Untuk skema database sudah terbuat dengan ORM oleh Spring Data JPA. Jika tidak terbuat skema database, bisa menjalankan script yang ada pada folder project `/employee-demo/database/ddl/employee_demo.sql`

## Deploy
Ketika hasil build sudah terbuat. Maka jar yang ada pada folder target bisa dijalankan menggunakan perintah ```java -jar emp-0.0.1-SNAPSHOT.jar```

## API Docs
API Documentation pada aplikasi ini menggunakan Swagger-UI bisa diakses melalui http://localhost:8080/swagger-ui.html.
Catatan untuk format LocalDate pada JSON menggunakan format **yyyy-MM-dd**
