package com.astralife.emp.repository;

import com.astralife.emp.model.Employee;
import org.jeasy.random.EasyRandom;
import org.jeasy.random.EasyRandomParameters;
import org.jeasy.random.FieldPredicates;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@DataJpaTest
@Transactional
class EmployeeRepositoryTest {
    @Autowired
    private EmployeeRepository employeeRepository;

    private EasyRandom easyRandom;
    private Employee employee;

    @BeforeEach
    void setUp(){
        EasyRandomParameters easyRandomParameters = new EasyRandomParameters().scanClasspathForConcreteTypes(true);
        easyRandomParameters.excludeField(FieldPredicates.named("empNo").and(FieldPredicates.inClass(Employee.class)));
        easyRandomParameters.excludeField(FieldPredicates.named("departmentManagers").and(FieldPredicates.inClass(Employee.class)));
        easyRandomParameters.excludeField(FieldPredicates.named("departmentEmployees").and(FieldPredicates.inClass(Employee.class)));
        easyRandomParameters.excludeField(FieldPredicates.named("salaries").and(FieldPredicates.inClass(Employee.class)));
        easyRandomParameters.excludeField(FieldPredicates.named("titles").and(FieldPredicates.inClass(Employee.class)));
        easyRandom = new EasyRandom(easyRandomParameters);
        employee = easyRandom.nextObject(Employee.class);
    }

    @Test
    void givenNull_whenCreate_shouldThrowException(){
        Exception ex = assertThrows(InvalidDataAccessApiUsageException.class, () -> employeeRepository.save(null));
        assertThat(ex).isInstanceOf(InvalidDataAccessApiUsageException.class);
    }

    @Test
    void givenNull_whenFindByEmpNo_shouldNotPresent() {
        Optional<Employee> employeeOptional = employeeRepository.findByEmpNo(null);
        assertThat(employeeOptional.isPresent()).isFalse();
    }

    @Test
    void givenEmployee_whenSave_shouldSaveToRepository() {
        employeeRepository.save(employee);
        assertThat(employeeRepository.count()).isEqualTo(1);
    }

    @Test
    void givenEmptyLong_whenFindByEmpNo_shouldEmpty() {
        assertTrue(employeeRepository.findByEmpNo(Long.getLong("")).isEmpty());
    }

    @Test
    void givenEmpNo_whenFindByEmpNo_shouldGiveCorrectResult() {
        employee = easyRandom.nextObject(Employee.class);
        employeeRepository.save(employee);
        Optional<Employee> employeeOptional = employeeRepository.findByEmpNo(employee.getEmpNo());
        assertTrue(employeeOptional.isPresent());
        assertThat(employeeOptional.get().toString()).isEqualTo(employee.toString());
    }

    @Test
    void givenEmployee_whenDelete_shouldDeleted() {
        employee = easyRandom.nextObject(Employee.class);
        employeeRepository.save(employee);
        Optional<Employee> employeeOptional = employeeRepository.findByEmpNo(employee.getEmpNo());
        assertTrue(employeeOptional.isPresent());

        employeeRepository.delete(employee);
        employeeOptional = employeeRepository.findByEmpNo(employee.getEmpNo());
        assertTrue(employeeOptional.isEmpty());
    }
}
