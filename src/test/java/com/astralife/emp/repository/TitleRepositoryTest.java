package com.astralife.emp.repository;

import com.astralife.emp.enumeration.Gender;
import com.astralife.emp.model.Employee;
import com.astralife.emp.model.Title;
import org.apache.commons.lang3.RandomStringUtils;
import org.jeasy.random.EasyRandom;
import org.jeasy.random.EasyRandomParameters;
import org.jeasy.random.FieldPredicates;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@DataJpaTest
@Transactional
class TitleRepositoryTest {
    @Autowired
    private TitleRepository titleRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    private Title title;

    @BeforeEach
    void setUp(){
        EasyRandomParameters easyRandomParameters = new EasyRandomParameters().scanClasspathForConcreteTypes(true);
        easyRandomParameters.excludeField(FieldPredicates.named("empNo").and(FieldPredicates.inClass(Title.class)));
        easyRandomParameters.excludeField(FieldPredicates.named("employee").and(FieldPredicates.inClass(Title.class)));
        EasyRandom easyRandom = new EasyRandom(easyRandomParameters);
        title = easyRandom.nextObject(Title.class);

        Employee employee = createEmployee();
        title.setEmployee(employee);
        title.setEmpNo(employee.getEmpNo());
    }

    @Test
    void givenNull_whenCreate_shouldThrowException(){
        Exception ex = assertThrows(InvalidDataAccessApiUsageException.class, () -> titleRepository.save(null));
        assertThat(ex).isInstanceOf(InvalidDataAccessApiUsageException.class);
    }

    @Test
    void givenNull_whenFindByEmpNo_shouldEmpty() {
        List<Title> titleList = titleRepository.findByEmpNo(null);
        assertThat(titleList).isEmpty();
    }

    @Test
    void givenTitle_whenSave_shouldSaveToRepository() {
        titleRepository.save(title);
        assertThat(titleRepository.count()).isEqualTo(1);
    }

    @Test
    void givenEmptyLong_whenFindByEmpNo_shouldEmpty() {
        assertTrue(titleRepository.findByEmpNo(Long.getLong("")).isEmpty());
    }

    @Test
    void givenTitle_whenDelete_shouldDeleted() {
        titleRepository.save(title);
        List<Title> titleList = titleRepository.findByEmpNo(title.getEmpNo());
        assertThat(titleList).isNotEmpty();

        titleRepository.delete(title);
        titleList = titleRepository.findByEmpNo(title.getEmpNo());
        assertTrue(titleList.isEmpty());
    }

    private Employee createEmployee(){
        Employee employee = new Employee();
        employee.setFirstName(RandomStringUtils.random(3));
        employee.setLastName(RandomStringUtils.random(3));
        employee.setGender(Gender.F);
        employee.setHireDate(LocalDate.now());
        employee.setBirthDate(LocalDate.now());
        employeeRepository.save(employee);
        return employee;
    }
}
