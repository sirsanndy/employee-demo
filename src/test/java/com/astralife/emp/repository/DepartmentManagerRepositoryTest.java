package com.astralife.emp.repository;

import com.astralife.emp.enumeration.Gender;
import com.astralife.emp.model.Department;
import com.astralife.emp.model.DepartmentManager;
import com.astralife.emp.model.Employee;
import org.apache.commons.lang3.RandomStringUtils;
import org.jeasy.random.EasyRandom;
import org.jeasy.random.EasyRandomParameters;
import org.jeasy.random.FieldPredicates;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@DataJpaTest
@Transactional
class DepartmentManagerRepositoryTest {
    @Autowired
    private DepartmentManagerRepository departmentManagerRepository;

    @Autowired
    private DepartmentRepository departmentRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    private DepartmentManager departmentManager;
    private Employee employee;
    private Department department;

    @BeforeEach
    void setUp(){
        EasyRandomParameters easyRandomParameters = new EasyRandomParameters().scanClasspathForConcreteTypes(true);
        easyRandomParameters.excludeField(FieldPredicates.named("employee").and(FieldPredicates.inClass(DepartmentManager.class)));
        easyRandomParameters.excludeField(FieldPredicates.named("department").and(FieldPredicates.inClass(DepartmentManager.class)));
        EasyRandom easyRandom = new EasyRandom(easyRandomParameters);

        employee = createEmployee();
        department = createDepartment();
        departmentManager = easyRandom.nextObject(DepartmentManager.class);
        departmentManager.setDeptNo(department.getDeptNo());
        departmentManager.setEmpNo(employee.getEmpNo());
    }

    @Test
    void givenNull_whenCreate_shouldThrowException(){
        Exception ex = assertThrows(InvalidDataAccessApiUsageException.class, () -> departmentManagerRepository.save(null));
        assertThat(ex).isInstanceOf(InvalidDataAccessApiUsageException.class);
    }

    @Test
    void givenNull_whenFindByEmpNo_shouldNotPresent() {
        List<DepartmentManager> departmentManagers = departmentManagerRepository.findByEmpNo(null);
        assertThat(departmentManagers).isEmpty();
    }

    @Test
    void givenNull_whenFindByDeptNo_shouldNotPresent() {
        List<DepartmentManager> departmentManagers = departmentManagerRepository.findByDeptNo(null);
        assertThat(departmentManagers).isEmpty();
    }

    @Test
    void givenDepartmentManager_whenSave_shouldSaveToRepository() {
        departmentManagerRepository.save(departmentManager);
        assertThat(departmentManagerRepository.count()).isEqualTo(1);
    }

    @Test
    void givenEmptyLong_whenFindByEmpNoAndFindbyDeptNo_shouldEmpty() {
        assertTrue(departmentManagerRepository.findByEmpNo(Long.getLong("")).isEmpty());
        assertTrue(departmentManagerRepository.findByDeptNo(Long.getLong("")).isEmpty());
    }

    @Test
    void givenDeptNo_whenFindByDeptNo_shouldGiveCorrectResult() {
        departmentManagerRepository.save(departmentManager);
        List<DepartmentManager> departmentManagers = departmentManagerRepository.findByDeptNo(departmentManager.getDeptNo());
        assertNotNull(departmentManagers);
        assertThat(departmentManagers).isNotEmpty();
    }

    @Test
    void givenDepartmentManager_whenDelete_shouldDeleted() {
        departmentManagerRepository.save(departmentManager);
        List<DepartmentManager> departmentManagers = departmentManagerRepository.findByDeptNo(department.getDeptNo());
        assertFalse(departmentManagers.isEmpty());

        departmentManagerRepository.delete(departmentManager);
        departmentManagers = departmentManagerRepository.findByDeptNo(department.getDeptNo());
        assertTrue(departmentManagers.isEmpty());
    }

    private Employee createEmployee(){
        employee = new Employee();
        employee.setFirstName(RandomStringUtils.random(20));
        employee.setLastName(RandomStringUtils.random(20));
        employee.setGender(Gender.F);
        employee.setHireDate(LocalDate.now());
        employee.setBirthDate(LocalDate.now());
        employeeRepository.save(employee);
        return employee;
    }

    private Department createDepartment(){
        department = new Department();
        department.setDeptName(RandomStringUtils.random(20));
        departmentRepository.save(department);
        return department;
    }
}
