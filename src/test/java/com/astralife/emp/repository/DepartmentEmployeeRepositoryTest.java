package com.astralife.emp.repository;

import com.astralife.emp.enumeration.Gender;
import com.astralife.emp.model.Department;
import com.astralife.emp.model.DepartmentEmployee;
import com.astralife.emp.model.Employee;
import org.apache.commons.lang3.RandomStringUtils;
import org.jeasy.random.EasyRandom;
import org.jeasy.random.EasyRandomParameters;
import org.jeasy.random.FieldPredicates;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@DataJpaTest
@Transactional
class DepartmentEmployeeRepositoryTest {
    @Autowired
    private DepartmentEmployeeRepository departmentEmployeeRepository;

    @Autowired
    private DepartmentRepository departmentRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    private DepartmentEmployee departmentEmployee;
    private Employee employee;
    private Department department;

    @BeforeEach
    void setUp(){
        EasyRandomParameters easyRandomParameters = new EasyRandomParameters().scanClasspathForConcreteTypes(true);
        easyRandomParameters.excludeField(FieldPredicates.named("employee").and(FieldPredicates.inClass(DepartmentEmployee.class)));
        easyRandomParameters.excludeField(FieldPredicates.named("department").and(FieldPredicates.inClass(DepartmentEmployee.class)));
        EasyRandom easyRandom = new EasyRandom(easyRandomParameters);

        employee = createEmployee();
        department = createDepartment();
        departmentEmployee = easyRandom.nextObject(DepartmentEmployee.class);
        departmentEmployee.setDeptNo(department.getDeptNo());
        departmentEmployee.setEmpNo(employee.getEmpNo());
    }

    @Test
    void givenNull_whenCreate_shouldThrowException(){
        Exception ex = assertThrows(InvalidDataAccessApiUsageException.class, () -> departmentEmployeeRepository.save(null));
        assertThat(ex).isInstanceOf(InvalidDataAccessApiUsageException.class);
    }

    @Test
    void givenNull_whenFindByEmpNo_shouldNotPresent() {
        List<DepartmentEmployee> departmentEmployees = departmentEmployeeRepository.findByEmpNo(null);
        assertThat(departmentEmployees).isEmpty();
    }

    @Test
    void givenNull_whenFindByDeptNo_shouldNotPresent() {
        List<DepartmentEmployee> departmentEmployees = departmentEmployeeRepository.findByDeptNo(null);
        assertThat(departmentEmployees).isEmpty();
    }

    @Test
    void givenDepartmentEmployee_whenSave_shouldSaveToRepository() {
        departmentEmployeeRepository.save(departmentEmployee);
        assertThat(departmentEmployeeRepository.count()).isEqualTo(1);
    }

    @Test
    void givenEmptyLong_whenFindByEmpNoAndFindbyDeptNo_shouldEmpty() {
        assertTrue(departmentEmployeeRepository.findByEmpNo(Long.getLong("")).isEmpty());
        assertTrue(departmentEmployeeRepository.findByDeptNo(Long.getLong("")).isEmpty());
    }

    @Test
    void givenDeptNo_whenFindByDeptNo_shouldGiveCorrectResult() {
        departmentEmployeeRepository.save(departmentEmployee);
        List<DepartmentEmployee> departmentEmployees = departmentEmployeeRepository.findByDeptNo(departmentEmployee.getDeptNo());
        assertNotNull(departmentEmployees);
        assertThat(departmentEmployees).isNotEmpty();
    }

    @Test
    void givenDepartmentEmployee_whenDelete_shouldDeleted() {
        departmentEmployeeRepository.save(departmentEmployee);
        List<DepartmentEmployee> departmentEmployees = departmentEmployeeRepository.findByDeptNo(department.getDeptNo());
        assertFalse(departmentEmployees.isEmpty());

        departmentEmployeeRepository.delete(departmentEmployee);
        departmentEmployees = departmentEmployeeRepository.findByDeptNo(department.getDeptNo());
        assertTrue(departmentEmployees.isEmpty());
    }
    private Employee createEmployee(){
        employee = new Employee();
        employee.setFirstName(RandomStringUtils.random(20));
        employee.setLastName(RandomStringUtils.random(20));
        employee.setGender(Gender.F);
        employee.setHireDate(LocalDate.now());
        employee.setBirthDate(LocalDate.now());
        employeeRepository.save(employee);
        return employee;
    }

    private Department createDepartment(){
        department = new Department();
        department.setDeptName(RandomStringUtils.random(20));
        departmentRepository.save(department);
        return department;
    }
}
