package com.astralife.emp.repository;

import com.astralife.emp.model.Department;
import org.jeasy.random.EasyRandom;
import org.jeasy.random.EasyRandomParameters;
import org.jeasy.random.FieldPredicates;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@DataJpaTest
@Transactional
class DepartmentRepositoryTest {
    @Autowired
    private DepartmentRepository departmentRepository;

    private EasyRandom easyRandom;
    private Department department;

    @BeforeEach
    void setUp(){
        EasyRandomParameters easyRandomParameters = new EasyRandomParameters().scanClasspathForConcreteTypes(true);
        easyRandomParameters.excludeField(FieldPredicates.named("deptNo").and(FieldPredicates.inClass(Department.class)));
        easyRandomParameters.excludeField(FieldPredicates.named("departmentManagers").and(FieldPredicates.inClass(Department.class)));
        easyRandomParameters.excludeField(FieldPredicates.named("departmentEmployees").and(FieldPredicates.inClass(Department.class)));
        easyRandom = new EasyRandom(easyRandomParameters);
        department = easyRandom.nextObject(Department.class);
    }

    @Test
    void givenNull_whenCreate_shouldThrowException(){
        Exception ex = assertThrows(InvalidDataAccessApiUsageException.class, () -> departmentRepository.save(null));
        assertThat(ex).isInstanceOf(InvalidDataAccessApiUsageException.class);
    }

    @Test
    void givenNull_whenFindByDeptNo_shouldNotPresent() {
        Optional<Department> departmentOptional = departmentRepository.findByDeptNo(null);
        assertThat(departmentOptional.isPresent()).isFalse();
    }

    @Test
    void givenDepartment_whenSave_shouldSaveToRepository() {
        departmentRepository.save(department);
        assertThat(departmentRepository.count()).isEqualTo(1);
    }

    @Test
    void givenEmptyLong_whenFindByEmpNo_shouldEmpty() {
        assertTrue(departmentRepository.findByDeptNo(Long.getLong("")).isEmpty());
    }

    @Test
    void givenDeptNo_whenFindByDeptNo_shouldGiveCorrectResult() {
        department = easyRandom.nextObject(Department.class);
        departmentRepository.save(department);
        Optional<Department> departmentOptional = departmentRepository.findByDeptNo(department.getDeptNo());
        assertTrue(departmentOptional.isPresent());
        assertThat(departmentOptional.get().toString()).isEqualTo(department.toString());
    }

    @Test
    void givenDepartment_whenDelete_shouldDeleted() {
        department = easyRandom.nextObject(Department.class);
        departmentRepository.saveAndFlush(department);
        Optional<Department> departmentOptional = departmentRepository.findByDeptNo(department.getDeptNo());
        assertTrue(departmentOptional.isPresent());

        departmentRepository.delete(department);
        departmentOptional = departmentRepository.findByDeptNo(department.getDeptNo());
        assertTrue(departmentOptional.isEmpty());
    }
}
