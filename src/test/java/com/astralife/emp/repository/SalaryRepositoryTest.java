package com.astralife.emp.repository;

import com.astralife.emp.enumeration.Gender;
import com.astralife.emp.model.Employee;
import com.astralife.emp.model.Salary;
import org.apache.commons.lang3.RandomStringUtils;
import org.jeasy.random.EasyRandom;
import org.jeasy.random.EasyRandomParameters;
import org.jeasy.random.FieldPredicates;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Optional;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@DataJpaTest
@Transactional
class SalaryRepositoryTest {
    @Autowired
    private SalaryRepository salaryRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    private Salary salary;

    @BeforeEach
    void setUp(){
        EasyRandomParameters easyRandomParameters = new EasyRandomParameters().scanClasspathForConcreteTypes(true);
        easyRandomParameters.excludeField(FieldPredicates.named("empNo").and(FieldPredicates.inClass(Salary.class)));
        easyRandomParameters.excludeField(FieldPredicates.named("employee").and(FieldPredicates.inClass(Salary.class)));
        EasyRandom easyRandom = new EasyRandom(easyRandomParameters);
        salary = easyRandom.nextObject(Salary.class);
        salary.setFromDate(LocalDate.of(2021,11,1));

        Employee employee = createEmployee();
        salary.setEmployee(employee);
        salary.setEmpNo(employee.getEmpNo());

    }

    @Test
    void givenNull_whenCreate_shouldThrowException(){
        Exception ex = assertThrows(InvalidDataAccessApiUsageException.class, () -> salaryRepository.save(null));
        assertThat(ex).isInstanceOf(InvalidDataAccessApiUsageException.class);
    }

    @Test
    void givenNull_whenFindByEmpNoAndFromDate_shouldEmpty() {
        Optional<Salary> salaryOptional = salaryRepository.findByEmpNoAndFromDate(null, null);
        assertThat(salaryOptional.isEmpty()).isTrue();
    }

    @Test
    void givenSalary_whenSave_shouldSaveToRepository() {
        salaryRepository.save(salary);
        assertThat(salaryRepository.count()).isEqualTo(1);
    }

    @Test
    void givenEmptyLongAndLocalDateNow_whenFindByEmpNo_shouldEmpty() {
        assertTrue(salaryRepository.findByEmpNoAndFromDate(Long.getLong(""), LocalDate.now()).isEmpty());
    }

    @Test
    void givenSalary_whenDelete_shouldDeleted() {
        salaryRepository.save(salary);
        Optional<Salary> salaryOptional = salaryRepository.findByEmpNoAndFromDate(salary.getEmpNo(), LocalDate.of(2021,11,1));
        assertThat(salaryOptional.isEmpty()).isFalse();

        salaryRepository.delete(salary);
        salaryOptional = salaryRepository.findByEmpNoAndFromDate(salary.getEmpNo(), LocalDate.of(2021,11,1));
        assertTrue(salaryOptional.isEmpty());
    }

    private Employee createEmployee(){
        Employee employee = new Employee();
        employee.setFirstName(RandomStringUtils.random(3));
        employee.setLastName(RandomStringUtils.random(3));
        employee.setGender(Gender.F);
        employee.setHireDate(LocalDate.now());
        employee.setBirthDate(LocalDate.now());
        employeeRepository.save(employee);
        return employee;
    }
}
