package com.astralife.emp.model;

import org.jeasy.random.EasyRandom;
import org.jeasy.random.EasyRandomParameters;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.time.LocalDate;
import java.util.Set;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ActiveProfiles("test")
@SpringBootTest
@DirtiesContext
class DepartmentManagerTest {
    @SpyBean
    private Validator validator;

    private DepartmentManager departmentManager;
    private Set<ConstraintViolation<DepartmentManager>> violations;

    @BeforeEach
    void setUp() {
        EasyRandomParameters easyRandomParameters = new EasyRandomParameters().scanClasspathForConcreteTypes(true);
        EasyRandom easyRandom = new EasyRandom(easyRandomParameters);
        departmentManager = easyRandom.nextObject(DepartmentManager.class);
    }

    @Test
    void givenDepartmentManager_whenValidate_shouldValid() {
        violations = validateDepartmentManager(departmentManager);
        assertTrue(violations.isEmpty());
    }

    @Test
    void givenDepartmentManagerNullDepartmentManagerEmpNo_whenValidate_shouldViolateTheValidation() {
        departmentManager.setEmpNo(null);
        violations = validateDepartmentManager(departmentManager);
        assertFalse(violations.isEmpty());
        assertThat(violations.size()).isEqualTo(1);
    }

    @Test
    void givenDepartmentManagerNullDepartmentManagerDeptNo_whenValidate_shouldViolateTheValidation() {
        departmentManager.setDeptNo(null);
        violations = validateDepartmentManager(departmentManager);
        assertFalse(violations.isEmpty());
        assertThat(violations.size()).isEqualTo(1);
    }

    @Test
    void givenDepartmentManagerNullDepartmentManagerFromDate_whenValidate_shouldViolateTheValidation() {
        departmentManager.setFromDate(null);
        violations = validateDepartmentManager(departmentManager);
        assertFalse(violations.isEmpty());
        assertThat(violations.size()).isEqualTo(1);
    }

    @Test
    void givenDepartmentManagerNullDepartmentManagerToDate_whenValidate_shouldViolateTheValidation() {
        departmentManager.setToDate(null);
        violations = validateDepartmentManager(departmentManager);
        assertFalse(violations.isEmpty());
        assertThat(violations.size()).isEqualTo(1);
    }

    @Test
    void givenDepartmentManager_expectedSameValues(){
        Employee emp = new Employee();
        emp.setEmpNo(1L);
        emp.setFirstName("Astralife");
        emp.setHireDate(LocalDate.of(2021, 10, 1));

        Department department = new Department();
        department.setDeptNo(1L);
        department.setDeptName("IT");

        departmentManager.setEmpNo(1L);
        departmentManager.setDeptNo(1L);
        departmentManager.setToDate(LocalDate.of(2022, 1, 1));
        departmentManager.setFromDate(LocalDate.of(2021, 10, 1));
        departmentManager.setEmployee(emp);
        departmentManager.setDepartment(department);

        assertTrue(departmentManager.getEmpNo().equals(1L));
        assertTrue(departmentManager.getDeptNo().equals(1L));
        assertTrue(departmentManager.getToDate().isEqual(LocalDate.of(2022, 1, 1)));
        assertTrue(departmentManager.getFromDate().isEqual(LocalDate.of(2021, 10, 1)));
        assertThat(departmentManager.getDepartment()).isNotNull();
        assertThat(departmentManager.getEmployee()).isNotNull();
    }

    private Set<ConstraintViolation<DepartmentManager>> validateDepartmentManager(DepartmentManager departmentManager){
        return validator.validate(departmentManager);
    }
}

