package com.astralife.emp.model;

import org.jeasy.random.EasyRandom;
import org.jeasy.random.EasyRandomParameters;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.time.LocalDate;
import java.util.Set;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("test")
@SpringBootTest
@DirtiesContext
class DepartmentEmployeeTest {
    @SpyBean
    private Validator validator;

    private DepartmentEmployee departmentEmployee;
    private Set<ConstraintViolation<DepartmentEmployee>> violations;

    @BeforeEach
    void setUp() {
        EasyRandomParameters easyRandomParameters = new EasyRandomParameters().scanClasspathForConcreteTypes(true);
        EasyRandom easyRandom = new EasyRandom(easyRandomParameters);
        departmentEmployee = easyRandom.nextObject(DepartmentEmployee.class);
    }
    
    @Test
    void givenDepartmentEmployee_whenValidate_shouldValid() {
        violations = validateDepartmentEmployee(departmentEmployee);
        assertTrue(violations.isEmpty());
    }

    @Test
    void givenDepartmentEmployeeNullDepartmentEmployeeEmpNo_whenValidate_shouldViolateTheValidation() {
        departmentEmployee.setEmpNo(null);
        violations = validateDepartmentEmployee(departmentEmployee);
        assertFalse(violations.isEmpty());
        assertThat(violations.size()).isEqualTo(1);
    }

    @Test
    void givenDepartmentEmployeeNullDepartmentEmployeeDeptNo_whenValidate_shouldViolateTheValidation() {
        departmentEmployee.setDeptNo(null);
        violations = validateDepartmentEmployee(departmentEmployee);
        assertFalse(violations.isEmpty());
        assertThat(violations.size()).isEqualTo(1);
    }

    @Test
    void givenDepartmentEmployeeNullDepartmentEmployeeFromDate_whenValidate_shouldViolateTheValidation() {
        departmentEmployee.setFromDate(null);
        violations = validateDepartmentEmployee(departmentEmployee);
        assertFalse(violations.isEmpty());
        assertThat(violations.size()).isEqualTo(1);
    }

    @Test
    void givenDepartmentEmployeeNullDepartmentEmployeeToDate_whenValidate_shouldViolateTheValidation() {
        departmentEmployee.setToDate(null);
        violations = validateDepartmentEmployee(departmentEmployee);
        assertFalse(violations.isEmpty());
        assertThat(violations.size()).isEqualTo(1);
    }

    @Test
    void givenDepartmentManager_expectedSameValues(){
        Employee emp = new Employee();
        emp.setEmpNo(1L);
        emp.setFirstName("Astralife");
        emp.setHireDate(LocalDate.of(2021, 10, 1));

        Department department = new Department();
        department.setDeptNo(1L);
        department.setDeptName("IT");

        departmentEmployee.setEmpNo(1L);
        departmentEmployee.setDeptNo(1L);
        departmentEmployee.setToDate(LocalDate.of(2022, 1, 1));
        departmentEmployee.setFromDate(LocalDate.of(2021, 10, 1));
        departmentEmployee.setEmployee(emp);
        departmentEmployee.setDepartment(department);

        assertEquals(departmentEmployee.getEmpNo(), 1L);
        assertEquals(departmentEmployee.getDeptNo(), 1L);
        assertTrue(departmentEmployee.getToDate().isEqual(LocalDate.of(2022, 1, 1)));
        assertTrue(departmentEmployee.getFromDate().isEqual(LocalDate.of(2021, 10, 1)));
        assertThat(departmentEmployee.getDepartment()).isNotNull();
        assertThat(departmentEmployee.getEmployee()).isNotNull();
    }
    
    private Set<ConstraintViolation<DepartmentEmployee>> validateDepartmentEmployee(DepartmentEmployee departmentEmployee) {
        return validator.validate(departmentEmployee);
    }
}
