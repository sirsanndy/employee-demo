package com.astralife.emp.model;

import org.jeasy.random.EasyRandom;
import org.jeasy.random.EasyRandomParameters;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.time.LocalDate;
import java.util.Set;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("test")
@SpringBootTest
@DirtiesContext
class TitleTest {

    @SpyBean
    private Validator validator;

    private Title title;
    private Set<ConstraintViolation<Title>> violations;

    @BeforeEach
    void setUp(){
        EasyRandomParameters easyRandomParameters = new EasyRandomParameters().scanClasspathForConcreteTypes(true);
        EasyRandom easyRandom = new EasyRandom(easyRandomParameters);
        title = easyRandom.nextObject(Title.class);
    }

    @Test
    void givenTitle_whenValidate_shouldValid() {
        violations = validateTitle(title);
        assertTrue(violations.isEmpty());
    }

    @Test
    void givenTitleNullTitleEmpNo_whenValidate_shouldViolateTheValidation() {
        title.setEmpNo(null);
        violations = validateTitle(title);
        assertFalse(violations.isEmpty());
        assertThat(violations.size()).isEqualTo(1);
    }

    @Test
    void givenTitleNullTitleFromDate_whenValidate_shouldViolateTheValidation() {
        title.setFromDate(null);
        violations = validateTitle(title);
        assertFalse(violations.isEmpty());
        assertThat(violations.size()).isEqualTo(1);
    }

    @Test
    void givenTitle_expectedSameValues(){
        Employee emp = new Employee();
        emp.setEmpNo(1L);
        emp.setFirstName("Astralife");
        emp.setHireDate(LocalDate.of(2021, 10, 1));

        title.setEmpNo(1L);
        title.setTitle("IT Programmer");
        title.setToDate(LocalDate.of(2022, 1, 1));
        title.setFromDate(LocalDate.of(2021, 10, 1));
        title.setEmployee(emp);

        assertEquals(1L, (long) title.getEmpNo());
        assertEquals("IT Programmer", title.getTitle());
        assertTrue(title.getToDate().isEqual(LocalDate.of(2022, 1, 1)));
        assertTrue(title.getFromDate().isEqual(LocalDate.of(2021, 10, 1)));
        assertThat(title.getEmployee()).isNotNull();
    }

    private Set<ConstraintViolation<Title>> validateTitle(Title title) {
        return validator.validate(title);
    }
}
