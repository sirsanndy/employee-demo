package com.astralife.emp.model;

import org.apache.commons.lang3.StringUtils;
import org.jeasy.random.EasyRandom;
import org.jeasy.random.EasyRandomParameters;
import org.jeasy.random.FieldPredicates;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("test")
@SpringBootTest
@DirtiesContext
class DepartmentTest {
    @SpyBean
    private Validator validator;

    private Department department;
    private Set<ConstraintViolation<Department>> violations;

    @BeforeEach
    void setUp() {
        EasyRandomParameters easyRandomParameters = new EasyRandomParameters().scanClasspathForConcreteTypes(true);
        easyRandomParameters.excludeField(FieldPredicates.named("departmentManagers").and(FieldPredicates.inClass(Department.class)));
        easyRandomParameters.excludeField(FieldPredicates.named("departmentEmployees").and(FieldPredicates.inClass(Department.class)));
        EasyRandom easyRandom = new EasyRandom(easyRandomParameters);
        department = easyRandom.nextObject(Department.class);
    }

    @Test
    void givenDepartment_whenValidate_shouldValid() {
        violations = validateDepartment(department);
        assertTrue(violations.isEmpty());
    }

    @Test
    void givenDepartmentNullOrEmptyDepartmentName_whenValidate_shouldViolateTheValidation() {
        department.setDeptName(null);
        violations = validateDepartment(department);
        assertFalse(violations.isEmpty());
        assertThat(violations.size()).isEqualTo(1);

        department.setDeptName(StringUtils.EMPTY);
        violations = validateDepartment(department);
        assertFalse(violations.isEmpty());
    }

    @Test
    void givenDepartment_shouldExpectSameValues(){
        EasyRandomParameters easyRandomParameters = new EasyRandomParameters().scanClasspathForConcreteTypes(true);
        EasyRandom easyRandom = new EasyRandom(easyRandomParameters);
        DepartmentManager departmentManager = easyRandom.nextObject(DepartmentManager.class);

        DepartmentEmployee departmentEmployee = easyRandom.nextObject(DepartmentEmployee.class);

        List<DepartmentManager> departmentManagerList = new ArrayList<>();
        departmentManagerList.add(departmentManager);

        List<DepartmentEmployee> departmentEmployeeList = new ArrayList<>();
        departmentEmployeeList.add(departmentEmployee);

        department.setDepartmentManagers(departmentManagerList);
        department.setDepartmentEmployees(departmentEmployeeList);

        assertNotNull(department.getDeptNo());
        assertNotNull(department.getDeptName());
        assertNotNull(department.getDepartmentEmployees());
        assertThat(department.getDepartmentEmployees()).hasSize(1);
        assertNotNull(department.getDepartmentManagers());
        assertThat(department.getDepartmentManagers()).hasSize(1);
        assertThat(department.toString()).isNotNull();
    }

    private Set<ConstraintViolation<Department>> validateDepartment(Department department){
        return validator.validate(department);
    }
}
