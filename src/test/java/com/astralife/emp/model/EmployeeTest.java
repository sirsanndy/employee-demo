package com.astralife.emp.model;

import com.astralife.emp.enumeration.Gender;
import org.apache.commons.lang3.StringUtils;
import org.jeasy.random.EasyRandom;
import org.jeasy.random.EasyRandomParameters;
import org.jeasy.random.FieldPredicates;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("test")
@SpringBootTest
@DirtiesContext
class EmployeeTest {
    @SpyBean
    private Validator validator;

    private Employee employee;
    private Set<ConstraintViolation<Employee>> violations;

    @BeforeEach
    void setUp() {
        EasyRandomParameters easyRandomParameters = new EasyRandomParameters().scanClasspathForConcreteTypes(true);
        easyRandomParameters.excludeField(FieldPredicates.named("departmentManagers")
                .and(FieldPredicates.named("departmentEmployees"))
                .and(FieldPredicates.named("salaries"))
                .and(FieldPredicates.named("titles"))
                .and(FieldPredicates.inClass(Employee.class)));
        EasyRandom easyRandom = new EasyRandom(easyRandomParameters);
        employee = easyRandom.nextObject(Employee.class);
    }

    @Test
    void givenEmployee_whenValidate_shouldValid() {
        violations = validateEmployee(employee);
        assertTrue(violations.isEmpty());
    }

    @Test
    void givenEmployeeNullOrEmptyEmployeeFirstName_whenValidate_shouldViolateTheValidation() {
        employee.setFirstName(null);
        violations = validateEmployee(employee);
        assertFalse(violations.isEmpty());
        assertThat(violations.size()).isEqualTo(1);

        employee.setFirstName(StringUtils.EMPTY);
        violations = validateEmployee(employee);
        assertFalse(violations.isEmpty());
    }

    @Test
    void givenEmployeeNullOrEmptyEmployeeLastName_whenValidate_shouldViolateTheValidation() {
        employee.setLastName(null);
        violations = validateEmployee(employee);
        assertFalse(violations.isEmpty());
        assertThat(violations.size()).isEqualTo(1);

        employee.setFirstName(StringUtils.EMPTY);
        violations = validateEmployee(employee);
        assertFalse(violations.isEmpty());
    }

    @Test
    void givenEmployeeNullEmployeeBirthDate_whenValidate_shouldViolateTheValidation() {
        employee.setBirthDate(null);
        violations = validateEmployee(employee);
        assertFalse(violations.isEmpty());
        assertThat(violations.size()).isEqualTo(1);
    }

    @Test
    void givenEmployeeNullEmployeeGender_whenValidate_shouldViolateTheValidation() {
        employee.setGender(null);
        violations = validateEmployee(employee);
        assertFalse(violations.isEmpty());
        assertThat(violations.size()).isEqualTo(1);
    }

    @Test
    void givenEmployeeNullEmployeeHireDate_whenValidate_shouldViolateTheValidation() {
        employee.setHireDate(null);
        violations = validateEmployee(employee);
        assertFalse(violations.isEmpty());
        assertThat(violations.size()).isEqualTo(1);
    }

    @Test
    void givenEmployee_shouldExpectSameValues(){
        EasyRandomParameters easyRandomParameters = new EasyRandomParameters().scanClasspathForConcreteTypes(true);
        EasyRandom easyRandom = new EasyRandom(easyRandomParameters);
        DepartmentEmployee departmentEmployee = easyRandom.nextObject(DepartmentEmployee.class);

        DepartmentManager departmentManager = easyRandom.nextObject(DepartmentManager.class);

        Salary salary = easyRandom.nextObject(Salary.class);

        Title title = easyRandom.nextObject(Title.class);

        List<DepartmentEmployee> departmentEmployeeList = new ArrayList<>();
        departmentEmployeeList.add(departmentEmployee);

        List<DepartmentManager> departmentManagerList = new ArrayList<>();
        departmentManagerList.add(departmentManager);

        List<Salary> salaryList = new ArrayList<>();
        salaryList.add(salary);

        List<Title> titleList = new ArrayList<>();
        titleList.add(title);

        employee.setGender(Gender.M);
        employee.setDepartmentEmployees(departmentEmployeeList);
        employee.setDepartmentManagers(departmentManagerList);
        employee.setSalaries(salaryList);
        employee.setTitles(titleList);

        assertThat(employee.getGender()).isEqualTo(Gender.M);
        assertThat(employee).isInstanceOf(Employee.class);
        assertThat(employee.getEmpNo()).isNotNull();
        assertThat(employee.getEmpNo()).isInstanceOf(Long.class);
        assertThat(employee.getBirthDate()).isNotNull();
        assertThat(employee.getBirthDate()).isInstanceOf(LocalDate.class);
        assertThat(employee.getFirstName()).isNotNull();
        assertThat(employee.getFirstName()).isInstanceOf(String.class);
        assertThat(employee.getLastName()).isNotNull();
        assertThat(employee.getLastName()).isInstanceOf(String.class);
        assertThat(employee.getGender()).isNotNull();
        assertThat(employee.getGender()).isInstanceOf(Gender.class);
        assertThat(employee.getHireDate()).isNotNull();
        assertThat(employee.getHireDate()).isInstanceOf(LocalDate.class);
        assertThat(employee.getDepartmentEmployees()).isNotNull();
        assertThat(employee.getDepartmentEmployees()).isInstanceOf(List.class);
        assertThat(employee.getSalaries()).isNotNull();
        assertThat(employee.getSalaries()).isInstanceOf(List.class);
        assertThat(employee.getTitles()).isNotNull();
        assertThat(employee.getTitles()).isInstanceOf(List.class);

        assertThat(employee.getDepartmentEmployees()).hasSize(1);
        assertThat(employee.getDepartmentManagers()).hasSize(1);
        assertThat(employee.getSalaries()).hasSize(1);
        assertThat(employee.getTitles()).hasSize(1);

        assertNotNull(employee.toString());
        assertThat(employee.toString()).isInstanceOf(String.class);
    }

    private Set<ConstraintViolation<Employee>> validateEmployee(Employee employee){
        return validator.validate(employee);
    }
}
