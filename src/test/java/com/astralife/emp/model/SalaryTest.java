package com.astralife.emp.model;

import org.jeasy.random.EasyRandom;
import org.jeasy.random.EasyRandomParameters;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.time.LocalDate;
import java.util.Set;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ActiveProfiles("test")
@SpringBootTest
@DirtiesContext
class SalaryTest {

    @SpyBean
    private Validator validator;

    private Salary salary;
    private Set<ConstraintViolation<Salary>> violations;

    @BeforeEach
    void setUp(){
        EasyRandomParameters easyRandomParameters = new EasyRandomParameters().scanClasspathForConcreteTypes(true);
        EasyRandom easyRandom = new EasyRandom(easyRandomParameters);
        salary = easyRandom.nextObject(Salary.class);
    }

    @Test
    void givenSalary_whenValidate_shouldValid() {
        violations = validateSalary(salary);
        assertTrue(violations.isEmpty());
    }

    @Test
    void givenSalaryNullSalaryEmpNo_whenValidate_shouldViolateTheValidation() {
        salary.setEmpNo(null);
        violations = validateSalary(salary);
        assertFalse(violations.isEmpty());
        assertThat(violations.size()).isEqualTo(1);
    }
    
    @Test
    void givenSalaryNullSalaryFromDate_whenValidate_shouldViolateTheValidation() {
        salary.setFromDate(null);
        violations = validateSalary(salary);
        assertFalse(violations.isEmpty());
        assertThat(violations.size()).isEqualTo(1);
    }

    @Test
    void givenSalaryNullSalaryToDate_whenValidate_shouldViolateTheValidation() {
        salary.setToDate(null);
        violations = validateSalary(salary);
        assertFalse(violations.isEmpty());
        assertThat(violations.size()).isEqualTo(1);
    }

    @Test
    void givenSalary_expectedSameValues(){
        Employee emp = new Employee();
        emp.setEmpNo(1L);
        emp.setFirstName("Astralife");
        emp.setHireDate(LocalDate.of(2021, 10, 1));

        salary.setEmpNo(1L);
        salary.setSalary(100000000L);
        salary.setToDate(LocalDate.of(2022, 1, 1));
        salary.setFromDate(LocalDate.of(2021, 10, 1));
        salary.setEmployee(emp);

        assertTrue(salary.getEmpNo().equals(1L));
        assertTrue(salary.getSalary().equals(100000000L));
        assertTrue(salary.getToDate().isEqual(LocalDate.of(2022, 1, 1)));
        assertTrue(salary.getFromDate().isEqual(LocalDate.of(2021, 10, 1)));
        assertThat(salary.getEmployee()).isNotNull();
    }

    private Set<ConstraintViolation<Salary>> validateSalary(Salary salary) {
        return validator.validate(salary);
    }
}
