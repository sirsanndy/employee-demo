package com.astralife.emp.controller;

import static org.mockito.BDDMockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;

import com.astralife.emp.constant.EmployeeConstant;
import com.astralife.emp.constant.SalaryConstant;
import com.astralife.emp.dto.EmployeeDto;
import com.astralife.emp.dto.SalaryDto;
import com.astralife.emp.enumeration.Gender;
import com.astralife.emp.model.Employee;
import com.astralife.emp.model.Salary;
import com.astralife.emp.service.EmployeeService;
import com.astralife.emp.service.SalaryService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.jeasy.random.EasyRandom;
import org.jeasy.random.EasyRandomParameters;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static com.google.common.truth.Truth.assertThat;

@ActiveProfiles("test")
@WebMvcTest(SalaryController.class)
class SalaryControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private SalaryService salaryService;

    @MockBean
    private EmployeeService employeeService;

    @Autowired
    private Validator validator;

    private Salary salary;

    private List<Salary> salaryList;

    @BeforeEach
    void setUp(){
        EasyRandomParameters easyRandomParameters = new EasyRandomParameters().scanClasspathForConcreteTypes(true);
        EasyRandom easyRandom = new EasyRandom(easyRandomParameters);
        salary = easyRandom.nextObject(Salary.class);
        salaryList = easyRandom.objects(Salary.class, 5)
                .collect(Collectors.toList());
    }

    @Test
    void whenFindAll_shouldFetchAllSalaries() throws Exception {
        given(salaryService.findAll()).willReturn(salaryList.stream()
                .map(slr -> modelMapper.map(slr, SalaryDto.class)).collect(Collectors.toList()));
        this.mockMvc.perform(get("/api/salary"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()", is(salaryList.size())));
    }

    @Test
    void givenExistingSalary_findByEmpNoAndFromDate_shouldFoundOne() throws Exception {
        given(salaryService.findByEmpNoAndFromDate(Mockito.anyLong(), Mockito.any(LocalDate.class))).willReturn(Optional.of(modelMapper.map(salary, SalaryDto.class)));

        this.mockMvc.perform(get("/api/salary/{empNo}/{fromDate}", RandomUtils.nextLong(), LocalDate.now()))
                .andExpect(status().isFound())
                .andExpect(jsonPath("$.salary", is(salary.getSalary())))
                .andExpect(jsonPath("$.toDate", is(salary.getToDate().toString())));
    }

    @Test
    void givenNonExistingSalary_findByEmpNoAndFromDate_shouldReturn404() throws Exception {
        given(salaryService.findByEmpNoAndFromDate(Mockito.anyLong(), Mockito.any(LocalDate.class))).willReturn(Optional.empty());

        this.mockMvc.perform(get("/api/salary/{empNo}/{fromDate}", RandomUtils.nextLong(), LocalDate.now()))
                .andExpect(status().isNotFound());
    }

    @Test
    void givenValidSalary_whenCreate_shouldCreated() throws Exception {
        Employee employee = new Employee();
        employee.setEmpNo(1L);
        employee.setHireDate(LocalDate.now());
        employee.setGender(Gender.M);
        employee.setFirstName(RandomStringUtils.random(5));
        employee.setLastName(RandomStringUtils.random(5));
        employee.setBirthDate(LocalDate.now());
        employee.setSalaries(Stream.of(salary).collect(Collectors.toList()));
        salary.setEmpNo(employee.getEmpNo());

        given(employeeService.findByEmpNo(Mockito.anyLong())).willReturn(Optional.of(modelMapper.map(employee, EmployeeDto.class)));
        given(salaryService.save(Mockito.any(SalaryDto.class))).willReturn(Optional.of(modelMapper.map(salary, SalaryDto.class)));

        this.mockMvc.perform(post("/api/salary")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(modelMapper.map(salary, SalaryDto.class))))
                .andExpect(status().isCreated());
    }

    @Test
    void givenInvalidSalary_whenCreate_shouldReturn400BadRequest() throws Exception {
        Employee employee = new Employee();
        employee.setEmpNo(1L);
        employee.setHireDate(LocalDate.now());
        employee.setGender(Gender.M);
        employee.setFirstName(RandomStringUtils.random(5));
        employee.setLastName(RandomStringUtils.random(5));
        employee.setBirthDate(LocalDate.now());
        employee.setSalaries(Stream.of(salary).collect(Collectors.toList()));

        salary.setFromDate(null);

        final Set<ConstraintViolation<Object>> result = validator.validate(salary);
        given(employeeService.findByEmpNo(Mockito.anyLong())).willReturn(Optional.of(modelMapper.map(employee, EmployeeDto.class)));
        given(salaryService.save(Mockito.any(SalaryDto.class))).willThrow(new ConstraintViolationException(result));

        this.mockMvc.perform(post("/api/salary")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(modelMapper.map(salary, SalaryDto.class))))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.error", is("Bad Request")))
                .andExpect(jsonPath("$.violations", hasSize(1)));
    }

    @Test
    void givenNonExistingEmployee_whenCreate_shouldReturn404() throws Exception {
        given(employeeService.findByEmpNo(Mockito.anyLong())).willReturn(Optional.empty());

        MvcResult result = this.mockMvc.perform(post("/api/salary")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(modelMapper.map(salary, SalaryDto.class))))
                .andExpect(status().isNotFound())
                .andReturn();

        assertThat(result.getResponse().getContentAsString()).isEqualTo(EmployeeConstant.EMPLOYEE_NOT_FOUND);
    }

    @Test
    void givenExistingSalary_whenDelete_shouldDeleted() throws Exception {
        given(salaryService.findByEmpNoAndFromDate(Mockito.anyLong(), Mockito.any(LocalDate.class)))
                .willReturn(Optional.of(modelMapper.map(salary, SalaryDto.class)));
        given(salaryService.delete(Mockito.any(SalaryDto.class))).willReturn(Optional.of(modelMapper.map(salary, SalaryDto.class)));

        this.mockMvc.perform(delete("/api/salary/{empNo}/{fromDate}", RandomUtils.nextLong(), LocalDate.now()))
                .andExpect(status().isOk());
    }
    @Test
    void givenNonExistingSalary_whenDelete_shouldReturn404() throws Exception {
        given(salaryService.findByEmpNoAndFromDate(Mockito.anyLong(), Mockito.any(LocalDate.class)))
                .willReturn(Optional.empty());

        MvcResult result = this.mockMvc.perform(delete("/api/salary/{empNo}/{fromDate}", RandomUtils.nextLong(), LocalDate.now()))
                .andExpect(status().isNotFound())
                .andReturn();

        assertThat(result.getResponse().getContentAsString()).isEqualTo(SalaryConstant.SALARY_NOT_FOUND);
    }

    @Test
    void givenExistingSalary_whenUpdate_shouldUpdated() throws Exception {
        given(salaryService.findByEmpNoAndFromDate(Mockito.anyLong(), Mockito.any(LocalDate.class)))
                .willReturn(Optional.of(modelMapper.map(salary, SalaryDto.class)));
        salary.setSalary(1000000000L);
        given(salaryService.update(Mockito.any(SalaryDto.class)))
                .willReturn(Optional.of(modelMapper.map(salary, SalaryDto.class)));

        this.mockMvc.perform(put("/api/salary/{empNo}/{fromDate}", RandomUtils.nextLong(), LocalDate.now())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(modelMapper.map(salary, SalaryDto.class))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.empNo", is(salary.getEmpNo())))
                .andExpect(jsonPath("$.salary", is(1000000000)))
                .andExpect(jsonPath("$.fromDate", is(salary.getFromDate().toString())))
                .andExpect(jsonPath("$.toDate", is(salary.getToDate().toString())));
    }

    @Test
    void givenNonExistingSalary_whenUpdate_shouldReturn404() throws Exception{
        given(salaryService.findByEmpNoAndFromDate(Mockito.anyLong(), Mockito.any(LocalDate.class)))
                .willReturn(Optional.empty());
        MvcResult result = this.mockMvc.perform(put("/api/salary/{empNo}/{fromDate}", RandomUtils.nextLong(), LocalDate.now())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(modelMapper.map(salary, SalaryDto.class))))
                .andExpect(status().isNotFound())
                .andReturn();

        assertThat(result.getResponse().getContentAsString()).isEqualTo(SalaryConstant.SALARY_NOT_FOUND);
    }

}