package com.astralife.emp.controller;

import com.astralife.emp.dto.DepartmentDto;
import com.astralife.emp.model.Department;
import com.astralife.emp.service.DepartmentService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.RandomUtils;
import org.jeasy.random.EasyRandom;
import org.jeasy.random.EasyRandomParameters;
import org.jeasy.random.FieldPredicates;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@WebMvcTest(DepartmentController.class)
class DepartmentControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private DepartmentService departmentService;

    @Autowired
    private Validator validator;

    private Department department;

    private List<Department> departmentList;

    @BeforeEach
    void setUp() {
        EasyRandomParameters easyRandomParameters = new EasyRandomParameters();
        easyRandomParameters.excludeField(FieldPredicates.named("deptNo").and(FieldPredicates.inClass(Department.class)));
        easyRandomParameters.excludeField(FieldPredicates.named("departmentManagers").and(FieldPredicates.inClass(Department.class)));
        easyRandomParameters.excludeField(FieldPredicates.named("departmentEmployees").and(FieldPredicates.inClass(Department.class)));
        EasyRandom easyRandom = new EasyRandom(easyRandomParameters);

        department = easyRandom.nextObject(Department.class);
        departmentList = easyRandom.objects(Department.class, 5)
                .collect(Collectors.toList());
    }

    @Test
    void whenFindAll_shouldFetchAllDepartment() throws Exception {
        given(departmentService.findAll()).willReturn(departmentList.stream().map(dept -> modelMapper.map(dept, DepartmentDto.class))
                .collect(Collectors.toList()));

        this.mockMvc.perform(get("/api/department"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()", is(departmentList.size())));
    }

    @Test
    void givenExistingDepartment_whenFindByDeptNo_shouldGiveCorrectResult() throws Exception {
        given(departmentService.findByDeptNo(Mockito.anyLong())).willReturn(Optional.of(modelMapper.map(department, DepartmentDto.class)));

        this.mockMvc.perform(get("/api/department/{deptNo}", RandomUtils.nextLong()))
                .andExpect(status().isFound())
                .andExpect(jsonPath("$.deptName", is(department.getDeptName())));
    }

    @Test
    void givenNonExistingDepartment_whenFindByDeptNo_shouldReturn404() throws Exception {
        given(departmentService.findByDeptNo(Mockito.anyLong())).willReturn(Optional.empty());

        this.mockMvc.perform(get("/api/department/{deptNo}", RandomUtils.nextLong()))
                .andExpect(status().isNotFound());
    }

    @Test
    void givenValidDepartment_whenCreate_shouldCreated() throws Exception {
        given(departmentService.save(Mockito.any(DepartmentDto.class))).willReturn(Optional.of(modelMapper.map(department, DepartmentDto.class)));

        this.mockMvc.perform(post("/api/department")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(modelMapper.map(department, DepartmentDto.class))))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.deptName", is(department.getDeptName())));
    }

    @Test
    void givenInvalidDepartment_whenCreate_should400BadRequest() throws Exception {
        department.setDeptName("");
        final Set<ConstraintViolation<Object>> result = validator.validate(department);
        given(departmentService.save(Mockito.any(DepartmentDto.class))).willThrow(new ConstraintViolationException(result));

        this.mockMvc.perform(post("/api/department")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(modelMapper.map(department, DepartmentDto.class))))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.error", is("Bad Request")))
                .andExpect(jsonPath("$.violations", hasSize(1)));
    }


    @Test
    void givenExistingDepartment_whenDeleteDepartment_shouldDeleted() throws Exception{
        given(departmentService.findByDeptNo(Mockito.anyLong())).willReturn(Optional.of(modelMapper.map(department, DepartmentDto.class)));

        given(departmentService.delete(Mockito.anyLong(), Mockito.any(DepartmentDto.class))).willReturn(Optional.of(modelMapper.map(department, DepartmentDto.class)));

        this.mockMvc.perform(delete("/api/department/{deptNo}", RandomUtils.nextLong()))
                .andExpect(status().isOk());
    }

    @Test
    void givenNonExistingDepartment_whenDeleteDepartment_shouldReturn404() throws Exception{
        given(departmentService.findByDeptNo(Mockito.anyLong())).willReturn(Optional.empty());

        this.mockMvc.perform(delete("/api/department/{deptNo}", RandomUtils.nextLong()))
                .andExpect(status().isNotFound());
    }

    @Test
    void givenExistingDepartment_whenUpdateDepartment_shouldReturn404() throws Exception {
        Department dept = department;
        dept.setDeptName("IT");

        given(departmentService.findByDeptNo(Mockito.anyLong())).willReturn(Optional.of(modelMapper.map(department, DepartmentDto.class)));

        given(departmentService.update(Mockito.anyLong(), Mockito.any(DepartmentDto.class))).willReturn(Optional.of(modelMapper.map(dept, DepartmentDto.class)));

        this.mockMvc.perform(put("/api/department/{deptNo}", RandomUtils.nextLong())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(modelMapper.map(department, DepartmentDto.class))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.deptName", is("IT")));
    }

    @Test
    void givenNonExistingDepartment_whenUpdateDepartment_shouldReturn404() throws Exception {
        given(departmentService.findByDeptNo(Mockito.anyLong())).willReturn(Optional.empty());

        this.mockMvc.perform(put("/api/department/{deptNo}", RandomUtils.nextLong())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(modelMapper.map(department, DepartmentDto.class))))
                .andExpect(status().isNotFound());
    }
}