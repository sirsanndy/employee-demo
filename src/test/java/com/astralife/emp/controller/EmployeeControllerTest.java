package com.astralife.emp.controller;

import static org.mockito.BDDMockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import com.astralife.emp.dto.EmployeeDto;
import com.astralife.emp.model.Employee;
import com.astralife.emp.service.EmployeeService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.RandomUtils;
import org.jeasy.random.EasyRandom;
import org.jeasy.random.EasyRandomParameters;
import org.jeasy.random.FieldPredicates;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@ActiveProfiles("test")
@WebMvcTest(EmployeeController.class)
class EmployeeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private EmployeeService employeeService;

    @Autowired private Validator validator;

    private Employee employee;

    private List<Employee> employees;

    @BeforeEach
    void setUp(){
        EasyRandomParameters easyRandomParameters = new EasyRandomParameters();
        easyRandomParameters.excludeField(FieldPredicates.named("departmentManagers").and(FieldPredicates.inClass(Employee.class)));
        easyRandomParameters.excludeField(FieldPredicates.named("departmentEmployees").and(FieldPredicates.inClass(Employee.class)));
        easyRandomParameters.excludeField(FieldPredicates.named("salaries").and(FieldPredicates.inClass(Employee.class)));
        easyRandomParameters.excludeField(FieldPredicates.named("titles").and(FieldPredicates.inClass(Employee.class)));
        EasyRandom generator = new EasyRandom(easyRandomParameters);
        employee = generator.nextObject(Employee.class);
        employees = generator.objects(Employee.class, 5)
                .collect(Collectors.toList());
    }

    @Test
    void whenFindAll_shouldFetchAllEmployees() throws Exception {
        given(employeeService.findAll()).willReturn(employees.stream().map(emp -> modelMapper.map(emp, EmployeeDto.class))
                .collect(Collectors.toList()));
        this.mockMvc.perform(get("/api/employee"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()", is(employees.size())));
    }

    @Test
    void whenFindByEmpNo_shouldFetchOneEmployee() throws Exception {
        given(employeeService.findByEmpNo(Mockito.any(Long.class))).willReturn(Optional.of(modelMapper.map(employee, EmployeeDto.class)));
        this.mockMvc.perform(get("/api/employee/{empNo}", RandomUtils.nextLong()))
                .andExpect(status().isFound())
                .andExpect(jsonPath("$.firstName", is(employee.getFirstName())))
                .andExpect(jsonPath("$.lastName", is(employee.getLastName())))
                .andExpect(jsonPath("$.birthDate", is(employee.getBirthDate().toString())))
                .andExpect(jsonPath("$.gender", is(employee.getGender().toString())))
                .andExpect(jsonPath("$.hireDate", is(employee.getHireDate().toString())));
    }

    @Test
    void whenFindByEmpNo_shouldReturn404() throws Exception {
        given(employeeService.findByEmpNo(Mockito.any(Long.class))).willReturn(Optional.empty());

        this.mockMvc.perform(get("/api/employee/{empNo}", RandomUtils.nextLong()))
                .andExpect(status().isNotFound());
    }

    @Test
    void givenValidEmployee_whenCreate_shouldCreated() throws Exception {
        given(employeeService.save(Mockito.any(EmployeeDto.class))).willReturn(Optional.of(modelMapper.map(employee, EmployeeDto.class)));

        this.mockMvc.perform(post("/api/employee/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(modelMapper.map(employee, EmployeeDto.class))))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.firstName", is(employee.getFirstName())))
                .andExpect(jsonPath("$.lastName", is(employee.getLastName())))
                .andExpect(jsonPath("$.birthDate", is(employee.getBirthDate().toString())))
                .andExpect(jsonPath("$.gender", is(employee.getGender().toString())))
                .andExpect(jsonPath("$.hireDate", is(employee.getHireDate().toString())));
    }

    @Test
    void givenInvalidEmployee_whenCreateNewEmployeeWithoutFirstNameAndHireDate_shouldReturn400() throws Exception {
        employee.setFirstName(null);
        employee.setHireDate(null);
        EmployeeDto employeeDto = modelMapper.map(employee, EmployeeDto.class);
        final Set<ConstraintViolation<Object>> result = validator.validate(employee);
        given(employeeService.save(Mockito.any(EmployeeDto.class))).willThrow(new ConstraintViolationException(result));

        this.mockMvc.perform(post("/api/employee")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(employeeDto)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.error", is("Bad Request")))
                .andExpect(jsonPath("$.violations", hasSize(2)));
    }

    @Test
    void giveExistingEmployee_whenDeleteEmployee_shouldDeleted() throws Exception {
        given(employeeService.findByEmpNo(Mockito.anyLong())).willReturn(Optional.of(modelMapper.map(employee, EmployeeDto.class)));

        given(employeeService.delete(Mockito.anyLong(), Mockito.any(EmployeeDto.class))).willReturn(Optional.of(modelMapper.map(employee, EmployeeDto.class)));

        this.mockMvc.perform(delete("/api/employee/{empNo}", RandomUtils.nextLong()))
                .andExpect(status().isOk());
    }

    @Test
    void givenNonExistingEmployee_whenDeleteEmployee_shouldReturn404() throws Exception {
        given(employeeService.findByEmpNo(Mockito.anyLong())).willReturn(Optional.empty());

        this.mockMvc.perform(delete("/api/employee/{empNo}", RandomUtils.nextLong()))
                .andExpect(status().isNotFound());
    }

    @Test
    void givenExistingEmployee_whenUpdateEmployee_shouldUpdated() throws Exception {
        Employee emp = employee;
        emp.setLastName("H");
        EmployeeDto employeeDto = modelMapper.map(employee, EmployeeDto.class);
        given(employeeService.findByEmpNo(Mockito.anyLong())).willReturn(Optional.of(modelMapper.map(employee, EmployeeDto.class)));

        given(employeeService.update(Mockito.anyLong(), Mockito.any(EmployeeDto.class))).willReturn(Optional.of(modelMapper.map(emp, EmployeeDto.class)));

        this.mockMvc.perform(put("/api/employee/{empNo}", RandomUtils.nextLong())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(employeeDto)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName", is(employee.getFirstName())))
                .andExpect(jsonPath("$.lastName", is(emp.getLastName())))
                .andExpect(jsonPath("$.birthDate", is(employee.getBirthDate().toString())))
                .andExpect(jsonPath("$.gender", is(employee.getGender().toString())))
                .andExpect(jsonPath("$.hireDate", is(employee.getHireDate().toString())));

    }

    @Test
    void givenNonExistingEmployee_whenUpdateEmployee_shouldReturn404() throws Exception {
        given(employeeService.findByEmpNo(Mockito.anyLong())).willReturn(Optional.empty());

        this.mockMvc.perform(put("/api/employee/{empNo}", RandomUtils.nextLong())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(modelMapper.map(employee, EmployeeDto.class))))
                .andExpect(status().isNotFound());
    }
}
