package com.astralife.emp.controller;

import com.astralife.emp.constant.DepartmentManagerConstant;
import com.astralife.emp.dto.DepartmentDto;
import com.astralife.emp.dto.DepartmentManagerDto;
import com.astralife.emp.dto.EmployeeDto;
import com.astralife.emp.enumeration.Gender;
import com.astralife.emp.model.Department;
import com.astralife.emp.model.DepartmentManager;
import com.astralife.emp.model.Employee;
import com.astralife.emp.service.DepartmentManagerService;
import com.astralife.emp.service.DepartmentService;
import com.astralife.emp.service.EmployeeService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.jeasy.random.EasyRandom;
import org.jeasy.random.EasyRandomParameters;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.google.common.truth.Truth.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@WebMvcTest(DepartmentManagerController.class)
class DepartmentManagerControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private DepartmentManagerService departmentManagerService;

    @MockBean
    private DepartmentService departmentService;

    @MockBean
    private EmployeeService employeeService;

    @Autowired
    private Validator validator;

    private DepartmentManager departmentManager;

    private List<DepartmentManager> departmentManagers;

    @BeforeEach
    void setUp(){
        EasyRandomParameters easyRandomParameters = new EasyRandomParameters().scanClasspathForConcreteTypes(true);
        EasyRandom easyRandom = new EasyRandom(easyRandomParameters);
        departmentManager = easyRandom.nextObject(DepartmentManager.class);
        departmentManagers = easyRandom.objects(DepartmentManager.class, 5)
                .collect(Collectors.toList());
    }

    @Test
    void whenFindAll_shouldFetchAllDepartmentManagers() throws Exception {
        given(departmentManagerService.findAll()).willReturn(departmentManagers.stream()
                .map(deptMgr -> modelMapper.map(deptMgr, DepartmentManagerDto.class)).collect(Collectors.toList()));

        this.mockMvc.perform(get("/api/deptmgr"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()", is(departmentManagers.size())));
    }

    @Test
    void givenExistingDepartmentManager_whenFindByDeptNoAndEmpNo_shouldFetchOne() throws Exception {
        given(departmentManagerService.findByEmpNoAndDeptNo(Mockito.anyLong(), Mockito.anyLong()))
                .willReturn(Optional.of(modelMapper.map(departmentManager, DepartmentManagerDto.class)));

        this.mockMvc.perform(get("/api/deptmgr/{empNo}/{deptNo}", RandomUtils.nextLong(), RandomUtils.nextLong()))
                .andExpect(status().isFound())
                .andExpect(jsonPath("$.empNo", is(departmentManager.getEmpNo())))
                .andExpect(jsonPath("$.deptNo", is(departmentManager.getDeptNo())))
                .andExpect(jsonPath("$.fromDate", is(departmentManager.getFromDate().toString())));
    }

    @Test
    void givenNonExistingDepartmentManager_whenFindByEmpNoAndDeptNo_shouldReturn404() throws Exception {
        given(departmentManagerService.findByEmpNoAndDeptNo(Mockito.anyLong(), Mockito.anyLong()))
                .willReturn(Optional.empty());

        this.mockMvc.perform(get("/api/deptmgr/{empNo}/{deptNo}", RandomUtils.nextLong(), RandomUtils.nextLong()))
                .andExpect(status().isNotFound());
    }

    @Test
    void givenValidDepartmentManager_whenCreate_shouldCreated() throws Exception {
        Employee employee = new Employee();
        employee.setEmpNo(1L);
        employee.setHireDate(LocalDate.now());
        employee.setGender(Gender.M);
        employee.setFirstName(RandomStringUtils.random(5));
        employee.setLastName(RandomStringUtils.random(5));
        employee.setBirthDate(LocalDate.now());
        employee.setDepartmentManagers(Stream.of(departmentManager).collect(Collectors.toList()));

        Department department = new Department();
        department.setDeptNo(1L);
        department.setDeptName("IT");
        department.setDepartmentManagers(Stream.of(departmentManager).collect(Collectors.toList()));

        given(employeeService.findByEmpNo(Mockito.anyLong())).willReturn(Optional.of(modelMapper.map(employee, EmployeeDto.class)));
        given(departmentService.findByDeptNo(Mockito.anyLong())).willReturn(Optional.of(modelMapper.map(department, DepartmentDto.class)));
        given(departmentManagerService.save(Mockito.any(DepartmentManagerDto.class)))
                .willReturn(Optional.of(modelMapper.map(departmentManager, DepartmentManagerDto.class)));

        this.mockMvc.perform(post("/api/deptmgr")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(modelMapper.map(departmentManager, DepartmentManagerDto.class))))
                .andExpect(status().isCreated());
    }

    @Test
    void givenInvalidDepartmentManager_whenCreate_shouldReturn400BadRequest() throws Exception {
        Employee employee = new Employee();
        employee.setEmpNo(1L);
        employee.setHireDate(LocalDate.now());
        employee.setGender(Gender.M);
        employee.setFirstName(RandomStringUtils.random(5));
        employee.setLastName(RandomStringUtils.random(5));
        employee.setBirthDate(LocalDate.now());
        employee.setDepartmentManagers(Stream.of(departmentManager).collect(Collectors.toList()));

        Department department = new Department();
        department.setDeptNo(1L);
        department.setDeptName("IT");
        department.setDepartmentManagers(Stream.of(departmentManager).collect(Collectors.toList()));

        departmentManager.setFromDate(null);

        final Set<ConstraintViolation<Object>> result = validator.validate(departmentManager);
        given(employeeService.findByEmpNo(Mockito.anyLong())).willReturn(Optional.of(modelMapper.map(employee, EmployeeDto.class)));
        given(departmentService.findByDeptNo(Mockito.anyLong())).willReturn(Optional.of(modelMapper.map(department, DepartmentDto.class)));
        given(departmentManagerService.save(Mockito.any(DepartmentManagerDto.class))).willThrow(new ConstraintViolationException(result));

        this.mockMvc.perform(post("/api/deptmgr")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(modelMapper.map(departmentManager, DepartmentManagerDto.class))))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.error", is("Bad Request")))
                .andExpect(jsonPath("$.violations", hasSize(1)));
    }
    
    @Test
    void givenExistingDepartmentManager_whenDelete_shouldDeleted() throws Exception {
        given(departmentManagerService.findByEmpNoAndDeptNo(Mockito.anyLong(), Mockito.anyLong()))
                .willReturn(Optional.of(modelMapper.map(departmentManager, DepartmentManagerDto.class)));
        given(departmentManagerService.delete(modelMapper.map(departmentManager, DepartmentManagerDto.class)))
                .willReturn(Optional.of(modelMapper.map(departmentManager, DepartmentManagerDto.class)));

        this.mockMvc.perform(delete("/api/deptmgr/{empNo}/{deptNo}", RandomUtils.nextLong(), RandomUtils.nextLong()))
                .andExpect(status().isOk());
    }

    @Test
    void givenNonExistingDepartmentManager_whenDelete_shouldReturn404() throws Exception {
        given(departmentManagerService.findByEmpNoAndDeptNo(Mockito.anyLong(), Mockito.anyLong()))
                .willReturn(Optional.empty());

        MvcResult result = this.mockMvc.perform(delete("/api/deptmgr/{empNo}/{deptNo}", RandomUtils.nextLong(), RandomUtils.nextLong()))
                .andExpect(status().isNotFound())
                .andReturn();

        assertThat(result.getResponse().getContentAsString()).isEqualTo(DepartmentManagerConstant.DEPARTMENT_MANAGER_NOT_FOUND);
    }

    @Test
    void givenExistingDepartmentManager_whenUpdate_shouldReturn404() throws Exception {
        departmentManager.setDeptNo(12L);
        departmentManager.setEmpNo(1L);
        given(departmentManagerService.findByEmpNoAndDeptNo(Mockito.anyLong(), Mockito.anyLong()))
                .willReturn(Optional.of(modelMapper.map(departmentManager, DepartmentManagerDto.class)));
        given(departmentManagerService.update(modelMapper.map(departmentManager, DepartmentManagerDto.class)))
                .willReturn(Optional.of(modelMapper.map(departmentManager, DepartmentManagerDto.class)));

        this.mockMvc.perform(put("/api/deptmgr/{empNo}/{deptNo}", RandomUtils.nextLong(), RandomUtils.nextLong())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(modelMapper.map(departmentManager, DepartmentManagerDto.class))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.deptNo", is(12)))
                .andExpect(jsonPath("$.empNo", is(1)));
    }

    @Test
    void givenNonExistingDepartmentManager_whenUpdate_shouldReturn404() throws Exception {
        given(departmentManagerService.findByEmpNoAndDeptNo(Mockito.anyLong(), Mockito.anyLong()))
                .willReturn(Optional.empty());

        MvcResult result = this.mockMvc.perform(put("/api/deptmgr/{empNo}/{deptNo}", RandomUtils.nextLong(), RandomUtils.nextLong())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(modelMapper.map(departmentManager, DepartmentManagerDto.class))))
                .andExpect(status().isNotFound())
                .andReturn();

        assertThat(result.getResponse().getContentAsString()).isEqualTo(DepartmentManagerConstant.DEPARTMENT_MANAGER_NOT_FOUND);
    }
}