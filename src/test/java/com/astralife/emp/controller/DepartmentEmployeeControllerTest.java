package com.astralife.emp.controller;

import static org.mockito.BDDMockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static com.google.common.truth.Truth.assertThat;

import com.astralife.emp.constant.DepartmentEmployeeConstant;
import com.astralife.emp.dto.DepartmentDto;
import com.astralife.emp.dto.DepartmentEmployeeDto;
import com.astralife.emp.dto.EmployeeDto;
import com.astralife.emp.enumeration.Gender;
import com.astralife.emp.model.Department;
import com.astralife.emp.model.DepartmentEmployee;
import com.astralife.emp.model.Employee;
import com.astralife.emp.service.DepartmentEmployeeService;
import com.astralife.emp.service.DepartmentService;
import com.astralife.emp.service.EmployeeService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.jeasy.random.EasyRandom;
import org.jeasy.random.EasyRandomParameters;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@ActiveProfiles("test")
@WebMvcTest(DepartmentEmployeeController.class)
class DepartmentEmployeeControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private DepartmentEmployeeService departmentEmployeeService;

    @MockBean
    private DepartmentService departmentService;

    @MockBean
    private EmployeeService employeeService;

    @Autowired
    private Validator validator;

    private DepartmentEmployee departmentEmployee;

    private List<DepartmentEmployee> departmentEmployees;

    @BeforeEach
    void setUp() {
        EasyRandomParameters easyRandomParameters = new EasyRandomParameters().scanClasspathForConcreteTypes(true);
        EasyRandom easyRandom = new EasyRandom(easyRandomParameters);
        departmentEmployee = easyRandom.nextObject(DepartmentEmployee.class);
        departmentEmployees = easyRandom.objects(DepartmentEmployee.class, 5)
                .collect(Collectors.toList());
    }

    @Test
    void whenFindAll_shouldFetchAllDepartmentEmployees() throws Exception {
        given(departmentEmployeeService.findAll()).willReturn(departmentEmployees.stream()
                .map(deptEmp -> modelMapper.map(deptEmp, DepartmentEmployeeDto.class)).collect(Collectors.toList()));

        this.mockMvc.perform(get("/api/deptemp"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()", is(departmentEmployees.size())));
    }

    @Test
    void givenExistingDepartmentEmployee_whenFindByDeptNoAndEmpNo_shouldFetchOne() throws Exception {
        given(departmentEmployeeService.findByDeptNoAndEmpNo(Mockito.anyLong(), Mockito.anyLong()))
                .willReturn(Optional.of(modelMapper.map(departmentEmployee, DepartmentEmployeeDto.class)));

        this.mockMvc.perform(get("/api/deptemp/{deptNo}/{empNo}", RandomUtils.nextLong(), RandomUtils.nextLong()))
                .andExpect(status().isFound())
                .andExpect(jsonPath("$.deptNo", is(departmentEmployee.getDeptNo())))
                .andExpect(jsonPath("$.empNo", is(departmentEmployee.getEmpNo())))
                .andExpect(jsonPath("$.fromDate", is(departmentEmployee.getFromDate().toString())));
    }

    @Test
    void givenNonExistingDepartmentEmployee_whenFindByDeptNoAndEmpNo_shouldReturn404() throws Exception {
        given(departmentEmployeeService.findByDeptNoAndEmpNo(Mockito.anyLong(), Mockito.anyLong()))
                .willReturn(Optional.empty());

        this.mockMvc.perform(get("/api/deptemp/{deptNo}/{empNo}", RandomUtils.nextLong(), RandomUtils.nextLong()))
                .andExpect(status().isNotFound());
    }

    @Test
    void givenValidDepartmentEmployee_whenCreate_shouldCreated() throws Exception {
        Employee employee = new Employee();
        employee.setEmpNo(1L);
        employee.setHireDate(LocalDate.now());
        employee.setGender(Gender.M);
        employee.setFirstName(RandomStringUtils.random(5));
        employee.setLastName(RandomStringUtils.random(5));
        employee.setBirthDate(LocalDate.now());
        employee.setDepartmentEmployees(Stream.of(departmentEmployee).collect(Collectors.toList()));

        Department department = new Department();
        department.setDeptNo(1L);
        department.setDeptName("IT");
        department.setDepartmentEmployees(Stream.of(departmentEmployee).collect(Collectors.toList()));

        given(employeeService.findByEmpNo(Mockito.anyLong())).willReturn(Optional.of(modelMapper.map(employee, EmployeeDto.class)));
        given(departmentService.findByDeptNo(Mockito.anyLong())).willReturn(Optional.of(modelMapper.map(department, DepartmentDto.class)));
        given(departmentEmployeeService.save(Mockito.any(DepartmentEmployeeDto.class)))
                .willReturn(Optional.of(modelMapper.map(departmentEmployee, DepartmentEmployeeDto.class)));

        this.mockMvc.perform(post("/api/deptemp")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(modelMapper.map(departmentEmployee, DepartmentEmployeeDto.class))))
                .andExpect(status().isCreated());
    }

    @Test
    void givenInvalidDepartmentEmployee_whenCreate_shouldReturn400BadRequest() throws Exception {
        Employee employee = new Employee();
        employee.setEmpNo(1L);
        employee.setHireDate(LocalDate.now());
        employee.setGender(Gender.M);
        employee.setFirstName(RandomStringUtils.random(5));
        employee.setLastName(RandomStringUtils.random(5));
        employee.setBirthDate(LocalDate.now());
        employee.setDepartmentEmployees(Stream.of(departmentEmployee).collect(Collectors.toList()));

        Department department = new Department();
        department.setDeptNo(1L);
        department.setDeptName("IT");
        department.setDepartmentEmployees(Stream.of(departmentEmployee).collect(Collectors.toList()));

        departmentEmployee.setFromDate(null);

        final Set<ConstraintViolation<Object>> result = validator.validate(departmentEmployee);
        given(employeeService.findByEmpNo(Mockito.anyLong())).willReturn(Optional.of(modelMapper.map(employee, EmployeeDto.class)));
        given(departmentService.findByDeptNo(Mockito.anyLong())).willReturn(Optional.of(modelMapper.map(department, DepartmentDto.class)));
        given(departmentEmployeeService.save(Mockito.any(DepartmentEmployeeDto.class))).willThrow(new ConstraintViolationException(result));

        this.mockMvc.perform(post("/api/deptemp")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(modelMapper.map(departmentEmployee, DepartmentEmployeeDto.class))))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.error", is("Bad Request")))
                .andExpect(jsonPath("$.violations", hasSize(1)));
    }

    @Test
    void givenExistingDepartmentEmployee_whenDelete_shouldDeleted() throws Exception {
        given(departmentEmployeeService.findByDeptNoAndEmpNo(Mockito.anyLong(), Mockito.anyLong()))
                .willReturn(Optional.of(modelMapper.map(departmentEmployee, DepartmentEmployeeDto.class)));
        given(departmentEmployeeService.delete(modelMapper.map(departmentEmployee, DepartmentEmployeeDto.class)))
                .willReturn(Optional.of(modelMapper.map(departmentEmployee, DepartmentEmployeeDto.class)));

        this.mockMvc.perform(delete("/api/deptemp/{deptNo}/{empNo}", RandomUtils.nextLong(), RandomUtils.nextLong()))
                .andExpect(status().isOk());
    }

    @Test
    void givenNonExistingDepartmentEmployee_whenDelete_shouldReturn404() throws Exception {
        given(departmentEmployeeService.findByDeptNoAndEmpNo(Mockito.anyLong(), Mockito.anyLong()))
                .willReturn(Optional.empty());

        MvcResult result = this.mockMvc.perform(delete("/api/deptemp/{deptNo}/{empNo}", RandomUtils.nextLong(), RandomUtils.nextLong()))
                .andExpect(status().isNotFound())
                .andReturn();

        assertThat(result.getResponse().getContentAsString()).isEqualTo(DepartmentEmployeeConstant.DEPARTMENT_EMPLOYEE_NOT_FOUND);
    }

    @Test
    void givenExistingDepartmentEmployee_whenUpdate_shouldReturn404() throws Exception {
        departmentEmployee.setDeptNo(12L);
        departmentEmployee.setEmpNo(1L);
        given(departmentEmployeeService.findByDeptNoAndEmpNo(Mockito.anyLong(), Mockito.anyLong()))
                .willReturn(Optional.of(modelMapper.map(departmentEmployee, DepartmentEmployeeDto.class)));
        given(departmentEmployeeService.update(modelMapper.map(departmentEmployee, DepartmentEmployeeDto.class)))
                .willReturn(Optional.of(modelMapper.map(departmentEmployee, DepartmentEmployeeDto.class)));

        this.mockMvc.perform(put("/api/deptemp/{deptNo}/{empNo}", RandomUtils.nextLong(), RandomUtils.nextLong())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(modelMapper.map(departmentEmployee, DepartmentEmployeeDto.class))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.deptNo", is(12)))
                .andExpect(jsonPath("$.empNo", is(1)));
    }

    @Test
    void givenNonExistingDepartmentEmployee_whenUpdate_shouldReturn404() throws Exception {
        given(departmentEmployeeService.findByDeptNoAndEmpNo(Mockito.anyLong(), Mockito.anyLong()))
                .willReturn(Optional.empty());

        MvcResult result = this.mockMvc.perform(put("/api/deptemp/{deptNo}/{empNo}", RandomUtils.nextLong(), RandomUtils.nextLong())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(modelMapper.map(departmentEmployee, DepartmentEmployeeDto.class))))
                .andExpect(status().isNotFound())
                .andReturn();

        assertThat(result.getResponse().getContentAsString()).isEqualTo(DepartmentEmployeeConstant.DEPARTMENT_EMPLOYEE_NOT_FOUND);
    }
}