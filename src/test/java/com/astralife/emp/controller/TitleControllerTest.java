package com.astralife.emp.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.BDDMockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static com.google.common.truth.Truth.assertThat;
import com.astralife.emp.constant.TitleConstant;
import com.astralife.emp.dto.EmployeeDto;
import com.astralife.emp.dto.TitleDto;
import com.astralife.emp.enumeration.Gender;
import com.astralife.emp.model.Employee;
import com.astralife.emp.model.Title;
import com.astralife.emp.service.EmployeeService;
import com.astralife.emp.service.TitleService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.jeasy.random.EasyRandom;
import org.jeasy.random.EasyRandomParameters;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@ActiveProfiles("test")
@WebMvcTest(TitleController.class)
class TitleControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private TitleService titleService;

    @MockBean
    private EmployeeService employeeService;

    @Autowired
    private Validator validator;

    private Title title;

    private List<Title> titleList;

    @BeforeEach
    void setUp() {
        EasyRandomParameters easyRandomParameters = new EasyRandomParameters().scanClasspathForConcreteTypes(true);
        EasyRandom easyRandom = new EasyRandom(easyRandomParameters);
        title = easyRandom.nextObject(Title.class);
        titleList = easyRandom.objects(Title.class, 5)
                .collect(Collectors.toList());
    }

    @Test
    void whenFindAll_shouldFetchAllTitles() throws Exception {
        given(titleService.findAll()).willReturn(titleList.stream()
                .map(t -> modelMapper.map(t, TitleDto.class)).collect(Collectors.toList()));

        this.mockMvc.perform(get("/api/title"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()", is(titleList.size())));
    }

    @Test
    void givenExistingTitle_whenFindByEmpNoAndFromDateAndTitle_shouldFoundOne() throws Exception {
        given(titleService.findByEmpNoAndFromDateAndTitle(Mockito.anyLong(), Mockito.any(LocalDate.class), Mockito.anyString()))
                .willReturn(Optional.of(modelMapper.map(title, TitleDto.class)));

        this.mockMvc.perform(get("/api/title/{empNo}/{fromDate}/{title}", RandomUtils.nextLong(), LocalDate.now(), RandomStringUtils.random(5)))
                .andExpect(status().isFound())
                .andExpect(jsonPath("$.empNo", is(title.getEmpNo())))
                .andExpect(jsonPath("$.title", is(title.getTitle())))
                .andExpect(jsonPath("$.fromDate", is(title.getFromDate().toString())))
                .andExpect(jsonPath("$.toDate", is(title.getToDate().toString())));
    }

    @Test
    void givenNonExistingTitle_whenFindByEmpNoAndFromDateAndTitle_shouldReturn404() throws Exception {
        given(titleService.findByEmpNoAndFromDateAndTitle(Mockito.anyLong(), Mockito.any(LocalDate.class), Mockito.anyString()))
                .willReturn(Optional.empty());

        this.mockMvc.perform(get("/api/title/{empNo}/{fromDate}/{title}", RandomUtils.nextLong(), LocalDate.now(), RandomStringUtils.random(5)))
                .andExpect(status().isNotFound());
    }

    @Test
    void whenGivenValidTitle_whenCreate_shouldCreated() throws Exception {
        Employee employee = new Employee();
        employee.setEmpNo(1L);
        employee.setHireDate(LocalDate.now());
        employee.setGender(Gender.M);
        employee.setFirstName(RandomStringUtils.random(5));
        employee.setLastName(RandomStringUtils.random(5));
        employee.setBirthDate(LocalDate.now());
        employee.setTitles(Stream.of(title).collect(Collectors.toList()));
        title.setEmpNo(employee.getEmpNo());
        given(employeeService.findByEmpNo(Mockito.anyLong())).willReturn(Optional.of(modelMapper.map(employee, EmployeeDto.class)));
        given(titleService.save(Mockito.any(TitleDto.class))).willReturn(Optional.of(modelMapper.map(title, TitleDto.class)));

        this.mockMvc.perform(post("/api/title")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(modelMapper.map(title, TitleDto.class))))
                .andExpect(status().isCreated());
    }

    @Test
    void givenInvalidTitle_whenCreate_shouldReturn400BadRequest() throws Exception {
        Employee employee = new Employee();
        employee.setEmpNo(1L);
        employee.setHireDate(LocalDate.now());
        employee.setGender(Gender.M);
        employee.setFirstName(RandomStringUtils.random(5));
        employee.setLastName(RandomStringUtils.random(5));
        employee.setBirthDate(LocalDate.now());
        employee.setTitles(Stream.of(title).collect(Collectors.toList()));

        title.setTitle("");
        title.setFromDate(null);

        final Set<ConstraintViolation<Object>> result = validator.validate(title);
        given(employeeService.findByEmpNo(Mockito.anyLong())).willReturn(Optional.of(modelMapper.map(employee, EmployeeDto.class)));
        given(titleService.save(Mockito.any(TitleDto.class))).willThrow(new ConstraintViolationException(result));

        this.mockMvc.perform(post("/api/title")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(modelMapper.map(title, TitleDto.class))))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.error", is("Bad Request")))
                .andExpect(jsonPath("$.violations", hasSize(2)));
    }

    @Test
    void givenExistingTitle_whenDelete_shouldDeleted() throws Exception {
        given(titleService.findByEmpNoAndFromDateAndTitle(Mockito.anyLong(), Mockito.any(LocalDate.class), Mockito.anyString()))
                .willReturn(Optional.of(modelMapper.map(title, TitleDto.class)));

        given(titleService.delete(Mockito.any(TitleDto.class)))
                .willReturn(Optional.of(modelMapper.map(title, TitleDto.class)));

        this.mockMvc.perform(delete("/api/title/{empNo}/{fromDate}/{title}", RandomUtils.nextLong(), LocalDate.now(), RandomStringUtils.random(5)))
                .andExpect(status().isOk());
    }

    @Test
    void givenNonExistingTitle_whenDelete_shouldReturn404() throws Exception {
        given(titleService.findByEmpNoAndFromDateAndTitle(Mockito.anyLong(), Mockito.any(LocalDate.class), Mockito.anyString()))
                .willReturn(Optional.empty());

        MvcResult result = this.mockMvc.perform(delete("/api/title/{empNo}/{fromDate}/{title}", RandomUtils.nextLong(), LocalDate.now(), RandomStringUtils.random(5)))
                .andExpect(status().isNotFound())
                .andReturn();

        assertThat(result.getResponse().getContentAsString()).isEqualTo(TitleConstant.TITLE_NOT_FOUND);
    }

    @Test
    void givenExistingTitle_whenUpdate_shouldUpdated() throws Exception {
        given(titleService.findByEmpNoAndFromDateAndTitle(Mockito.anyLong(), Mockito.any(LocalDate.class), Mockito.anyString()))
                .willReturn(Optional.of(modelMapper.map(title, TitleDto.class)));
        title.setTitle("IT Architect");
        given(titleService.update(Mockito.any(TitleDto.class))).willReturn(Optional.of(modelMapper.map(title, TitleDto.class)));

        this.mockMvc.perform(put("/api/title/{empNo}/{fromDate}/{title}", RandomUtils.nextLong(), LocalDate.now(), RandomStringUtils.random(5))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(modelMapper.map(title, TitleDto.class))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.empNo", is(title.getEmpNo())))
                .andExpect(jsonPath("$.title", is("IT Architect")))
                .andExpect(jsonPath("$.fromDate", is(title.getFromDate().toString())))
                .andExpect(jsonPath("$.toDate", is(title.getToDate().toString())));
    }

    @Test
    void givenNonExistingTitle_whenUpdate_shouldReturn404() throws Exception {
        given(titleService.findByEmpNoAndFromDateAndTitle(Mockito.anyLong(), Mockito.any(LocalDate.class), Mockito.anyString()))
                .willReturn(Optional.empty());

        MvcResult result = this.mockMvc.perform(put("/api/title/{empNo}/{fromDate}/{title}", RandomUtils.nextLong(), LocalDate.now(), RandomStringUtils.random(5))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(modelMapper.map(title, TitleDto.class))))
                .andExpect(status().isNotFound())
                .andReturn();

        assertThat(result.getResponse().getContentAsString()).isEqualTo(TitleConstant.TITLE_NOT_FOUND);
    }
}