package com.astralife.emp.service;

import com.astralife.emp.dto.DepartmentDto;
import com.astralife.emp.exception.DepartmentException;
import com.astralife.emp.model.Department;
import com.astralife.emp.repository.DepartmentRepository;
import com.astralife.emp.service.impl.DepartmentServiceImpl;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.jeasy.random.EasyRandom;
import org.jeasy.random.EasyRandomParameters;
import org.jeasy.random.FieldPredicates;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@SpringBootTest
class DepartmentServiceImplTest {
    @InjectMocks
    private DepartmentServiceImpl departmentServiceImpl;

    @Mock
    private DepartmentRepository departmentRepository;

    @Spy
    private ModelMapper modelMapper;

    private Department department;

    @BeforeEach
    void setUp(){
        department = new Department();
        department.setDeptNo(1L);
        department.setDeptName(RandomStringUtils.random(20));
    }

    @Test
    void givenNull_whenSave_thenThrowException(){
        Mockito.when(departmentRepository.save(null)).thenThrow(DepartmentException.class);
        Exception ex = assertThrows(DepartmentException.class, () -> departmentServiceImpl.save(null));
        assertThat(ex).isInstanceOf(DepartmentException.class);
    }

    @Test
    void givenValidDepartment_whenSave_thenShouldBeCreated(){
        DepartmentDto departmentDto = modelMapper.map(department, DepartmentDto.class);
        departmentServiceImpl.save(departmentDto);
        Mockito.verify(departmentRepository, Mockito.times(1)).save(Mockito.any(Department.class));
        assertThat(departmentServiceImpl.save(departmentDto)).isInstanceOf(Optional.class);
        assertTrue(departmentServiceImpl.save(departmentDto).isPresent());
    }

    @Test
    void givenValidDeptNo_whenFindByDeptNo_thenDeptShouldBeFound() {
        Mockito.when(departmentRepository.findByDeptNo(1L)).thenReturn(Optional.of(department));
        departmentServiceImpl.findByDeptNo(1L).ifPresent(departmentDto -> assertThat(departmentDto).isNotNull());
        assertThat(departmentServiceImpl.findByDeptNo(1L)).isInstanceOf(Optional.class);
        assertTrue(departmentServiceImpl.findByDeptNo(1L).isPresent());
    }

    @Test
    void givenValidDepartment_whenUpdate_thenShouldBeUpdated(){
        DepartmentDto departmentDto = modelMapper.map(department, DepartmentDto.class);
        Mockito.when(departmentRepository.getById(Mockito.anyLong())).thenReturn(department);
        Mockito.when(departmentRepository.save(Mockito.any(Department.class))).thenReturn(department);

        departmentServiceImpl.update(RandomUtils.nextLong(), departmentDto);
        Mockito.verify(departmentRepository, Mockito.times(1)).save(Mockito.any(Department.class));

        assertThat(departmentServiceImpl.update(RandomUtils.nextLong(), departmentDto).get()).isInstanceOf(DepartmentDto.class);
        assertTrue(departmentServiceImpl.update(RandomUtils.nextLong(), departmentDto).isPresent());
    }

    @Test
    void givenValidDepartment_whenDelete_thenShouldBeDeleted(){
        DepartmentDto departmentDto = modelMapper.map(department, DepartmentDto.class);
        Mockito.when(departmentRepository.getById(Mockito.any(Long.class))).thenReturn(department);

        departmentServiceImpl.delete(RandomUtils.nextLong(), departmentDto);
        Mockito.verify(departmentRepository, Mockito.times(1)).delete(Mockito.any(Department.class));

        assertThat(departmentServiceImpl.delete(RandomUtils.nextLong(), departmentDto).get()).isInstanceOf(DepartmentDto.class);
        assertTrue(departmentServiceImpl.delete(RandomUtils.nextLong(), departmentDto).isPresent());
    }

    @Test
    void givenValidEmpNo_whenDeleteById_thenShouldBeDeleted(){
        Mockito.when(departmentRepository.findByDeptNo(Mockito.any(Long.class))).thenReturn(Optional.ofNullable(department));

        departmentServiceImpl.deleteById(department.getDeptNo());
        Mockito.verify(departmentRepository, Mockito.times(1)).deleteById(Mockito.anyLong());

        assertThat(departmentServiceImpl.deleteById(RandomUtils.nextLong()).get()).isInstanceOf(DepartmentDto.class);
        assertTrue(departmentServiceImpl.deleteById(RandomUtils.nextLong()).isPresent());
    }

    @Test
    void whenFindAll_thenShouldLoadAllDepartment(){
        EasyRandomParameters easyRandomParameters = new EasyRandomParameters();
        easyRandomParameters.excludeField(FieldPredicates.named("departmentManagers").and(FieldPredicates.inClass(Department.class)));
        easyRandomParameters.excludeField(FieldPredicates.named("departmentEmployees").and(FieldPredicates.inClass(Department.class)));
        EasyRandom generator = new EasyRandom(easyRandomParameters);
        List<Department> departments = generator.objects(Department.class, 5)
                .collect(Collectors.toList());
        Mockito.when(departmentRepository.findAll()).thenReturn(departments);
        departments = departmentServiceImpl.findAll().stream()
                .map(departmentDto -> modelMapper.map(departmentDto, Department.class))
                .collect(Collectors.toList());
        assertThat(departments).isNotEmpty();
        assertFalse(departments.isEmpty());
        assertThat(departmentServiceImpl.findAll().size()).isEqualTo(5);
    }
}
