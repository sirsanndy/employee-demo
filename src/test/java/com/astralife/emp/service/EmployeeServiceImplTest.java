package com.astralife.emp.service;

import com.astralife.emp.dto.EmployeeDto;
import com.astralife.emp.enumeration.Gender;
import com.astralife.emp.exception.EmployeeException;
import com.astralife.emp.model.Employee;
import com.astralife.emp.repository.EmployeeRepository;
import com.astralife.emp.service.impl.EmployeeServiceImpl;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.jeasy.random.EasyRandom;
import org.jeasy.random.EasyRandomParameters;
import org.jeasy.random.FieldPredicates;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@SpringBootTest
class EmployeeServiceImplTest {
    @InjectMocks
    private EmployeeServiceImpl employeeServiceImpl;

    @Mock
    private EmployeeRepository employeeRepository;

    @Spy
    private ModelMapper modelMapper;

    private Employee employee;

    @BeforeEach
    void setUp(){
        employee = new Employee();
        employee.setEmpNo(123L);
        employee.setFirstName(RandomStringUtils.random(20));
        employee.setLastName(RandomStringUtils.random(20));
        employee.setGender(Gender.F);
        employee.setHireDate(LocalDate.now());
        employee.setBirthDate(LocalDate.now());
    }

    @Test
    void givenNull_whenSave_thenThrowException(){
        Mockito.when(employeeRepository.save(null)).thenThrow(EmployeeException.class);
        Exception ex = assertThrows(EmployeeException.class, () -> employeeServiceImpl.save(null));
        assertThat(ex).isInstanceOf(EmployeeException.class);
    }

    @Test
    void givenValidEmployee_whenSave_thenShouldBeCreated(){
        EmployeeDto employeeDto = modelMapper.map(employee, EmployeeDto.class);
        employeeServiceImpl.save(employeeDto);
        Mockito.verify(employeeRepository, Mockito.times(1)).save(Mockito.any(Employee.class));
    }

    @Test
    void givenValidEmpNo_whenFindByEmpNo_thenEmployeeShouldBeFound() {
        Mockito.when(employeeRepository.findByEmpNo(123L)).thenReturn(Optional.of(employee));
        employeeServiceImpl.findByEmpNo(123L).ifPresent(employeeDto -> assertThat(employeeDto).isNotNull());
    }

    @Test
    void givenValidEmployee_whenUpdate_thenShouldBeUpdated(){
        EmployeeDto employeeDto = modelMapper.map(employee, EmployeeDto.class);
        Mockito.when(employeeRepository.getById(Mockito.anyLong())).thenReturn(employee);
        Mockito.when(employeeRepository.save(Mockito.any(Employee.class))).thenReturn(employee);

        employeeServiceImpl.update(RandomUtils.nextLong(), employeeDto);
        Mockito.verify(employeeRepository, Mockito.times(1)).save(Mockito.any(Employee.class));
    }

    @Test
    void givenValidEmployee_whenDelete_thenShouldBeDeleted(){
        EmployeeDto employeeDto = modelMapper.map(employee, EmployeeDto.class);
        Mockito.when(employeeRepository.getById(Mockito.anyLong())).thenReturn(employee);

        employeeServiceImpl.delete(RandomUtils.nextLong(), employeeDto);
        Mockito.verify(employeeRepository, Mockito.times(1)).delete(Mockito.any(Employee.class));
    }

    @Test
    void givenValidEmpNo_whenDeleteById_thenShouldBeDeleted(){
        Mockito.when(employeeRepository.findByEmpNo(Mockito.anyLong())).thenReturn(Optional.ofNullable(employee));

        employeeServiceImpl.deleteById(employee.getEmpNo());
        Mockito.verify(employeeRepository, Mockito.times(1)).deleteById(Mockito.any(Long.class));
    }

    @Test
    void whenFindAll_thenEmployeesShouldBeFound(){
        EasyRandomParameters easyRandomParameters = new EasyRandomParameters();
        easyRandomParameters.excludeField(FieldPredicates.named("departmentManagers").and(FieldPredicates.inClass(Employee.class)));
        easyRandomParameters.excludeField(FieldPredicates.named("departmentEmployees").and(FieldPredicates.inClass(Employee.class)));
        easyRandomParameters.excludeField(FieldPredicates.named("salaries").and(FieldPredicates.inClass(Employee.class)));
        easyRandomParameters.excludeField(FieldPredicates.named("titles").and(FieldPredicates.inClass(Employee.class)));
        EasyRandom generator = new EasyRandom(easyRandomParameters);
        List<Employee> employees = generator.objects(Employee.class, 5)
                .collect(Collectors.toList());
        Mockito.when(employeeRepository.findAll()).thenReturn(employees);
        employees = employeeServiceImpl.findAll().stream()
                .map(employeeDto -> modelMapper.map(employeeDto, Employee.class))
                .collect(Collectors.toList());
        assertThat(employees).isNotEmpty();
        assertFalse(employees.isEmpty());
        assertThat(employees.size()).isEqualTo(5);
    }
}
