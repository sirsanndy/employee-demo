package com.astralife.emp.service;

import com.astralife.emp.dto.TitleDto;
import com.astralife.emp.enumeration.Gender;
import com.astralife.emp.exception.TitleException;
import com.astralife.emp.model.Employee;
import com.astralife.emp.model.Title;
import com.astralife.emp.repository.TitleRepository;
import com.astralife.emp.service.impl.TitleServiceImpl;
import org.apache.commons.lang3.RandomStringUtils;
import org.jeasy.random.EasyRandom;
import org.jeasy.random.EasyRandomParameters;
import org.jeasy.random.FieldPredicates;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@SpringBootTest
class TitleServiceImplTest {
    @InjectMocks
    private TitleServiceImpl titleServiceImpl;

    @Mock
    private TitleRepository titleRepository;

    @Spy
    private ModelMapper modelMapper;

    private Title title;

    @BeforeEach
    void setUp(){
        Employee employee = new Employee();
        employee.setEmpNo(1L);
        employee.setFirstName(RandomStringUtils.random(3));
        employee.setLastName(RandomStringUtils.random(3));
        employee.setGender(Gender.F);
        employee.setHireDate(LocalDate.now());
        employee.setBirthDate(LocalDate.now());

        title = new Title();
        title.setEmpNo(employee.getEmpNo());
        title.setTitle(RandomStringUtils.random(20));
        title.setFromDate(LocalDate.now());
        title.setToDate(LocalDate.now());
        title.setEmployee(employee);
    }

    @Test
    void givenNull_whenSave_thenThrowException(){
        Mockito.when(titleRepository.save(null)).thenThrow(TitleException.class);
        Exception ex = assertThrows(TitleException.class, () -> titleServiceImpl.save(null));
        assertThat(ex).isInstanceOf(TitleException.class);
    }

    @Test
    void givenValidTitle_whenSave_thenShouldBeCreated(){
        TitleDto titleDto = modelMapper.map(title, TitleDto.class);
        titleServiceImpl.save(titleDto);
        Mockito.verify(titleRepository, Mockito.times(1)).save(Mockito.any(Title.class));
    }

    @Test
    void givenValidEmpNoAndFromDateAndTitle_whenFindByEmpNoAndFromDateAndTitle_thenTitleShouldBeFound() {
        Mockito.when(titleRepository.findByEmpNoAndFromDateAndTitle(Mockito.anyLong(), Mockito.any(LocalDate.class), Mockito.any(String.class)))
                .thenReturn(Optional.of(title));
        titleServiceImpl.findByEmpNoAndFromDateAndTitle(1L, LocalDate.now(), RandomStringUtils.random(1)).stream().findAny().ifPresent(titleDto -> assertThat(titleDto.getEmpNo().toString()).isEqualTo("1"));
    }

    @Test
    void givenValidTitle_whenUpdate_thenShouldBeUpdated(){
        TitleDto titleDto = modelMapper.map(title, TitleDto.class);
        Mockito.when(titleRepository.findByEmpNoAndFromDateAndTitle(Mockito.anyLong(), Mockito.any(LocalDate.class), Mockito.any(String.class)))
                .thenReturn(Optional.of(title));
        Mockito.when(titleRepository.save(Mockito.any(Title.class))).thenReturn(title);

        titleServiceImpl.update(titleDto);
        Mockito.verify(titleRepository, Mockito.times(1)).save(Mockito.any(Title.class));
    }

    @Test
    void givenValidTitle_whenDelete_thenShouldBeDeleted(){
        TitleDto titleDto = modelMapper.map(title, TitleDto.class);
        Mockito.when(titleRepository.findByEmpNoAndFromDateAndTitle(Mockito.anyLong(), Mockito.any(LocalDate.class), Mockito.any(String.class)))
                .thenReturn(Optional.of(title));

        titleServiceImpl.delete(titleDto);
        Mockito.verify(titleRepository, Mockito.times(1)).delete(Mockito.any(Title.class));
    }

    @Test
    void whenFindAll_thenShouldLoadAllSalary(){
        EasyRandomParameters easyRandomParameters = new EasyRandomParameters();
        easyRandomParameters.excludeField(FieldPredicates.named("employee").and(FieldPredicates.inClass(Title.class)));
        EasyRandom generator = new EasyRandom(easyRandomParameters);
        List<Title> titles = generator.objects(Title.class, 5)
                .collect(Collectors.toList());
        Mockito.when(titleRepository.findAll()).thenReturn(titles);
        titles = titleServiceImpl.findAll().stream()
                .map(titleDto -> modelMapper.map(titleDto, Title.class))
                .collect(Collectors.toList());
        assertThat(titles).isNotEmpty();
        assertFalse(titles.isEmpty());
        assertThat(titles.size()).isEqualTo(5);
    }
}
