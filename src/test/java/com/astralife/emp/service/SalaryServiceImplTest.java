package com.astralife.emp.service;

import com.astralife.emp.dto.SalaryDto;
import com.astralife.emp.enumeration.Gender;
import com.astralife.emp.exception.SalaryException;
import com.astralife.emp.model.Employee;
import com.astralife.emp.model.Salary;
import com.astralife.emp.repository.SalaryRepository;
import com.astralife.emp.service.impl.SalaryServiceImpl;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.jeasy.random.EasyRandom;
import org.jeasy.random.EasyRandomParameters;
import org.jeasy.random.FieldPredicates;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@SpringBootTest
class SalaryServiceImplTest {
    @InjectMocks
    private SalaryServiceImpl salaryServiceImpl;

    @Mock
    private SalaryRepository salaryRepository;

    @Spy
    private ModelMapper modelMapper;

    private Salary salary;

    @BeforeEach
    void setUp(){
        Employee employee = new Employee();
        employee.setEmpNo(1L);
        employee.setFirstName(RandomStringUtils.random(3));
        employee.setLastName(RandomStringUtils.random(3));
        employee.setGender(Gender.F);
        employee.setHireDate(LocalDate.now());
        employee.setBirthDate(LocalDate.now());

        salary = new Salary();
        salary.setEmpNo(1L);
        salary.setSalary(RandomUtils.nextLong());
        salary.setFromDate(LocalDate.now());
        salary.setToDate(LocalDate.now());
        salary.setEmployee(employee);
    }

    @Test
    void givenNull_whenSave_thenThrowException(){
        Mockito.when(salaryRepository.save(null)).thenThrow(SalaryException.class);
        Exception ex = assertThrows(SalaryException.class, () -> salaryServiceImpl.save(null));
        assertThat(ex).isInstanceOf(SalaryException.class);
    }

    @Test
    void givenValidSalary_whenSave_thenShouldBeCreated(){
        SalaryDto salaryDto = modelMapper.map(salary, SalaryDto.class);
        salaryServiceImpl.save(salaryDto);
        Mockito.verify(salaryRepository, Mockito.times(1)).save(Mockito.any(Salary.class));
    }

    @Test
    void givenValidEmpNoAndFromDate_whenFindByEmpNoAndFromDate_thenSalaryShouldBeFound() {
        Mockito.when(salaryRepository.findByEmpNoAndFromDate(Mockito.anyLong(), Mockito.any(LocalDate.class))).thenReturn(Optional.of(salary));
        salaryServiceImpl.findByEmpNoAndFromDate(1L, LocalDate.now()).stream().findAny().ifPresent(salaryDto -> assertThat(salaryDto.getEmpNo().toString()).isEqualTo("1"));
    }

    @Test
    void givenValidSalary_whenUpdate_thenShouldBeUpdated(){
        SalaryDto salaryDto = modelMapper.map(salary, SalaryDto.class);
        Mockito.when(salaryRepository.findByEmpNoAndFromDate(Mockito.anyLong(), Mockito.any(LocalDate.class))).thenReturn(Optional.of(salary));
        Mockito.when(salaryRepository.save(Mockito.any(Salary.class))).thenReturn(salary);

        salaryServiceImpl.update(salaryDto);
        Mockito.verify(salaryRepository, Mockito.times(1)).save(Mockito.any(Salary.class));
    }

    @Test
    void givenValidSalary_whenDelete_thenShouldBeDeleted(){
        SalaryDto salaryDto = modelMapper.map(salary, SalaryDto.class);
        Mockito.when(salaryRepository.findByEmpNoAndFromDate(Mockito.anyLong(), Mockito.any(LocalDate.class))).thenReturn(Optional.of(salary));

        salaryServiceImpl.delete(salaryDto);
        Mockito.verify(salaryRepository, Mockito.times(1)).delete(Mockito.any(Salary.class));
    }

    @Test
    void whenFindAll_thenShouldLoadAllSalary(){
        EasyRandomParameters easyRandomParameters = new EasyRandomParameters();
        easyRandomParameters.excludeField(FieldPredicates.named("employee").and(FieldPredicates.inClass(Salary.class)));
        EasyRandom generator = new EasyRandom(easyRandomParameters);
        List<Salary> salaries = generator.objects(Salary.class, 5)
                .collect(Collectors.toList());
        Mockito.when(salaryRepository.findAll()).thenReturn(salaries);
        salaries = salaryServiceImpl.findAll().stream()
                .map(salaryDto -> modelMapper.map(salaryDto, Salary.class))
                .collect(Collectors.toList());
        assertThat(salaries).isNotEmpty();
        assertFalse(salaries.isEmpty());
        assertThat(salaries.size()).isEqualTo(5);
    }
}
