package com.astralife.emp.service;

import com.astralife.emp.dto.DepartmentManagerDto;
import com.astralife.emp.enumeration.Gender;
import com.astralife.emp.exception.DepartmentManagerException;
import com.astralife.emp.model.Department;
import com.astralife.emp.model.DepartmentManager;
import com.astralife.emp.model.Employee;
import com.astralife.emp.repository.DepartmentManagerRepository;
import com.astralife.emp.service.impl.DepartmentManagerServiceImpl;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.jeasy.random.EasyRandom;
import org.jeasy.random.EasyRandomParameters;
import org.jeasy.random.FieldPredicates;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@SpringBootTest
class DepartmentManagerServiceImplTest {
    @InjectMocks
    private DepartmentManagerServiceImpl departmentManagerServiceImpl;

    @Mock
    private DepartmentManagerRepository departmentManagerRepository;

    @Spy
    private ModelMapper modelMapper;

    private DepartmentManager departmentManager;

    @BeforeEach
    void setUp(){
        Employee employee = createEmployee();
        Department department = createDepartment();

        departmentManager = new DepartmentManager();
        departmentManager.setEmpNo(employee.getEmpNo());
        departmentManager.setDeptNo(department.getDeptNo());
        departmentManager.setFromDate(LocalDate.of(2021,11,1));
        departmentManager.setToDate(LocalDate.of(2021,12,1));
        departmentManager.setEmployee(employee);
        departmentManager.setDepartment(department);
    }

    @Test
    void givenNull_whenSave_thenThrowException(){
        Mockito.when(departmentManagerRepository.save(null)).thenThrow(DepartmentManagerException.class);
        Exception ex = assertThrows(DepartmentManagerException.class, () -> departmentManagerServiceImpl.save(null));
        assertThat(ex).isInstanceOf(DepartmentManagerException.class);
    }

    @Test
    void givenValidDepartmentManager_whenSave_thenShouldBeCreated(){
        DepartmentManagerDto departmentManagerDto = modelMapper.map(departmentManager, DepartmentManagerDto.class);
        departmentManagerServiceImpl.save(departmentManagerDto);
        Mockito.verify(departmentManagerRepository, Mockito.times(1)).save(Mockito.any(DepartmentManager.class));
    }

    @Test
    void givenValidDeptNo_whenFindByDeptNo_thenDepartmentManagerShouldBeFound() {
        Mockito.when(departmentManagerRepository.findByDeptNo(1L)).thenReturn(List.of(departmentManager));
        List<DepartmentManagerDto> departmentManagerDtoList = departmentManagerServiceImpl.findByDeptNo(1L);

        assertFalse(departmentManagerDtoList.isEmpty());
        assertThat(departmentManagerDtoList.size()).isEqualTo(1);
    }

    @Test
    void givenValidDepartmentManager_whenUpdate_thenShouldBeUpdated(){
        DepartmentManagerDto departmentManagerDto = modelMapper.map(departmentManager, DepartmentManagerDto.class);
        Mockito.when(departmentManagerRepository.findByEmpNoAndDeptNo(Mockito.anyLong(), Mockito.anyLong())).thenReturn(Optional.of(departmentManager));
        Mockito.when(departmentManagerRepository.save(Mockito.any(DepartmentManager.class))).thenReturn(departmentManager);

        departmentManagerServiceImpl.update(departmentManagerDto);
        Mockito.verify(departmentManagerRepository, Mockito.times(1)).save(Mockito.any(DepartmentManager.class));
    }

    @Test
    void givenValidDepartmentManager_whenDelete_thenShouldBeDeleted(){
        DepartmentManagerDto departmentManagerDto = modelMapper.map(departmentManager, DepartmentManagerDto.class);
        Mockito.when(departmentManagerRepository.findByEmpNoAndDeptNo(Mockito.anyLong(), Mockito.anyLong())).thenReturn(Optional.ofNullable(departmentManager));

        departmentManagerServiceImpl.delete(departmentManagerDto);
        Mockito.verify(departmentManagerRepository, Mockito.times(1)).delete(Mockito.any(DepartmentManager.class));
    }

    @Test
    void whenFindAll_thenShouldLoadAllDepartmentManager(){
        EasyRandomParameters easyRandomParameters = new EasyRandomParameters();
        easyRandomParameters.excludeField(FieldPredicates.named("employee").and(FieldPredicates.inClass(DepartmentManager.class)));
        easyRandomParameters.excludeField(FieldPredicates.named("department").and(FieldPredicates.inClass(DepartmentManager.class)));
        EasyRandom generator = new EasyRandom(easyRandomParameters);
        List<DepartmentManager> departmentManagers = generator.objects(DepartmentManager.class, 5)
                .collect(Collectors.toList());
        Mockito.when(departmentManagerRepository.findAll()).thenReturn(departmentManagers);
        departmentManagers = departmentManagerServiceImpl.findAll().stream()
                .map(departmentManagerDto -> modelMapper.map(departmentManagerDto, DepartmentManager.class))
                .collect(Collectors.toList());
        assertThat(departmentManagers).isNotEmpty();
        assertFalse(departmentManagers.isEmpty());
        assertThat(departmentManagers.size()).isEqualTo(5);
    }

    private Employee createEmployee(){
        Employee employee = new Employee();
        employee.setEmpNo(RandomUtils.nextLong());
        employee.setFirstName(RandomStringUtils.random(20));
        employee.setLastName(RandomStringUtils.random(20));
        employee.setGender(Gender.F);
        employee.setHireDate(LocalDate.now());
        employee.setBirthDate(LocalDate.now());
        return employee;
    }

    private Department createDepartment(){
        Department department = new Department();
        department.setDeptNo(RandomUtils.nextLong());
        department.setDeptName(RandomStringUtils.random(20));
        return department;
    }
}
