package com.astralife.emp.service;

import com.astralife.emp.dto.DepartmentEmployeeDto;
import com.astralife.emp.enumeration.Gender;
import com.astralife.emp.exception.DepartmentEmployeeException;
import com.astralife.emp.model.Department;
import com.astralife.emp.model.DepartmentEmployee;
import com.astralife.emp.model.Employee;
import com.astralife.emp.repository.DepartmentEmployeeRepository;
import com.astralife.emp.service.impl.DepartmentEmployeeServiceImpl;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.jeasy.random.EasyRandom;
import org.jeasy.random.EasyRandomParameters;
import org.jeasy.random.FieldPredicates;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@SpringBootTest
class DepartmentEmployeeServiceImplTest {
    @InjectMocks
    private DepartmentEmployeeServiceImpl departmentEmployeeServiceImpl;

    @Mock
    private DepartmentEmployeeRepository departmentEmployeeRepository;

    @Spy
    private ModelMapper modelMapper;

    private DepartmentEmployee departmentEmployee;

    @BeforeEach
    void setUp(){
        Department department = createDepartment();
        Employee employee = createEmployee();

        departmentEmployee = new DepartmentEmployee();
        departmentEmployee.setDeptNo(department.getDeptNo());
        departmentEmployee.setEmpNo(employee.getEmpNo());
        departmentEmployee.setFromDate(LocalDate.of(2021,11,1));
        departmentEmployee.setToDate(LocalDate.of(2021, 12, 1));
        departmentEmployee.setDepartment(department);
        departmentEmployee.setEmployee(employee);
    }

    @Test
    void givenNull_whenSave_thenThrowException(){
        Mockito.when(departmentEmployeeRepository.save(null)).thenThrow(DepartmentEmployeeException.class);
        Exception ex = assertThrows(DepartmentEmployeeException.class, () -> departmentEmployeeServiceImpl.save(null));
        assertThat(ex).isInstanceOf(DepartmentEmployeeException.class);
    }

    @Test
    void givenValidDepartmentEmployee_whenSave_thenShouldBeCreated(){
        DepartmentEmployeeDto departmentEmployeeDto = modelMapper.map(departmentEmployee, DepartmentEmployeeDto.class);
        departmentEmployeeServiceImpl.save(departmentEmployeeDto);
        Mockito.verify(departmentEmployeeRepository, Mockito.times(1)).save(Mockito.any(DepartmentEmployee.class));
    }

    @Test
    void givenValidDeptNo_whenFindByDeptNo_thenDepartmentEmployeeShouldBeFound() {
        Mockito.when(departmentEmployeeRepository.findByDeptNo(1L)).thenReturn(List.of(departmentEmployee));
        List<DepartmentEmployeeDto> departmentManagerDtoList = departmentEmployeeServiceImpl.findByDeptNo(1L);

        assertFalse(departmentManagerDtoList.isEmpty());
        assertThat(departmentManagerDtoList.size()).isEqualTo(1);
    }

    @Test
    void givenValidDepartmentEmployee_whenUpdate_thenShouldBeUpdated(){
        DepartmentEmployeeDto departmentManagerDto = modelMapper.map(departmentEmployee, DepartmentEmployeeDto.class);
        Mockito.when(departmentEmployeeRepository.findByDeptNoAndEmpNo(Mockito.anyLong(), Mockito.anyLong())).thenReturn(Optional.of(departmentEmployee));
        Mockito.when(departmentEmployeeRepository.save(Mockito.any(DepartmentEmployee.class))).thenReturn(departmentEmployee);

        departmentEmployeeServiceImpl.update(departmentManagerDto);
        Mockito.verify(departmentEmployeeRepository, Mockito.times(1)).save(Mockito.any(DepartmentEmployee.class));
    }

    @Test
    void givenValidDepartmentEmployee_whenDelete_thenShouldBeDeleted(){
        DepartmentEmployeeDto departmentManagerDto = modelMapper.map(departmentEmployee, DepartmentEmployeeDto.class);
        Mockito.when(departmentEmployeeRepository.findByDeptNoAndEmpNo(Mockito.anyLong(), Mockito.anyLong())).thenReturn(Optional.ofNullable(departmentEmployee));

        departmentEmployeeServiceImpl.delete(departmentManagerDto);
        Mockito.verify(departmentEmployeeRepository, Mockito.times(1)).delete(Mockito.any(DepartmentEmployee.class));
    }

    @Test
    void whenFindAll_thenShouldLoadAllDepartmentEmployee(){
        EasyRandomParameters easyRandomParameters = new EasyRandomParameters();
        easyRandomParameters.excludeField(FieldPredicates.named("employee").and(FieldPredicates.inClass(DepartmentEmployee.class)));
        easyRandomParameters.excludeField(FieldPredicates.named("department").and(FieldPredicates.inClass(DepartmentEmployee.class)));
        EasyRandom generator = new EasyRandom(easyRandomParameters);
        List<DepartmentEmployee> departmentEmployees = generator.objects(DepartmentEmployee.class, 5)
                .collect(Collectors.toList());
        Mockito.when(departmentEmployeeRepository.findAll()).thenReturn(departmentEmployees);
        departmentEmployees = departmentEmployeeServiceImpl.findAll().stream()
                .map(departmentManagerDto -> modelMapper.map(departmentManagerDto, DepartmentEmployee.class))
                .collect(Collectors.toList());
        assertThat(departmentEmployees).isNotEmpty();
        assertFalse(departmentEmployees.isEmpty());
        assertThat(departmentEmployees.size()).isEqualTo(5);
    }

    private Employee createEmployee(){
        Employee employee = new Employee();
        employee.setEmpNo(RandomUtils.nextLong());
        employee.setFirstName(RandomStringUtils.random(20));
        employee.setLastName(RandomStringUtils.random(20));
        employee.setGender(Gender.F);
        employee.setHireDate(LocalDate.now());
        employee.setBirthDate(LocalDate.now());
        return employee;
    }

    private Department createDepartment(){
        Department department = new Department();
        department.setDeptNo(RandomUtils.nextLong());
        department.setDeptName(RandomStringUtils.random(20));
        return department;
    }
}
