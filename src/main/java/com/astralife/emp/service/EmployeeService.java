package com.astralife.emp.service;

import com.astralife.emp.dto.EmployeeDto;
import com.astralife.emp.exception.EmployeeException;
import java.util.List;
import java.util.Optional;

public interface EmployeeService {
    Optional<EmployeeDto> findByEmpNo(Long empNo);

    Optional<EmployeeDto> save(EmployeeDto employeeDto);

    Optional<EmployeeDto> update(Long empNo, EmployeeDto employeeDto) throws EmployeeException;

    List<EmployeeDto> findAll();

    Optional<EmployeeDto> delete(Long empNo, EmployeeDto employeeDto);
}
