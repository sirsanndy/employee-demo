package com.astralife.emp.service;

import com.astralife.emp.dto.TitleDto;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface TitleService {
    Optional<TitleDto> save(TitleDto titleDto);

    Optional<TitleDto> findByEmpNoAndFromDateAndTitle(Long empNo, LocalDate fromDate, String title);

    Optional<TitleDto>  update(TitleDto titleDto);

    List<TitleDto> findAll();

    Optional<TitleDto> delete(TitleDto titleDto);
}
