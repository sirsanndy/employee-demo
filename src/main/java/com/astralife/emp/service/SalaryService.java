package com.astralife.emp.service;

import com.astralife.emp.dto.SalaryDto;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface SalaryService {
    Optional<SalaryDto> save(SalaryDto salaryDto);

    Optional<SalaryDto> findByEmpNoAndFromDate(Long empNo, LocalDate fromDate);

    Optional<SalaryDto> update(SalaryDto salaryDto);

    Optional<SalaryDto> delete(SalaryDto salaryDto);

    List<SalaryDto> findAll();
}
