package com.astralife.emp.service;

import com.astralife.emp.dto.DepartmentManagerDto;

import java.util.List;
import java.util.Optional;

public interface DepartmentManagerService {
    Optional<DepartmentManagerDto> save(DepartmentManagerDto departmentManagerDto);

    List<DepartmentManagerDto> findByDeptNo(Long deptNo);

    Optional<DepartmentManagerDto> findByEmpNoAndDeptNo(Long empNo, Long deptNo);

    Optional<DepartmentManagerDto> update(DepartmentManagerDto departmentManagerDto);

    Optional<DepartmentManagerDto> delete(DepartmentManagerDto departmentManagerDto);

    List<DepartmentManagerDto> findAll();
}
