package com.astralife.emp.service.impl;

import com.astralife.emp.constant.DepartmentConstant;
import com.astralife.emp.dto.DepartmentDto;
import com.astralife.emp.exception.DepartmentException;
import com.astralife.emp.model.Department;
import com.astralife.emp.repository.DepartmentRepository;
import com.astralife.emp.service.DepartmentService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class DepartmentServiceImpl implements DepartmentService {
    private final ModelMapper modelMapper;

    private final DepartmentRepository departmentRepository;

    @Autowired
    public DepartmentServiceImpl(ModelMapper modelMapper, DepartmentRepository departmentRepository) {
        this.modelMapper = modelMapper;
        this.departmentRepository = departmentRepository;
    }

    @Override
    public Optional<DepartmentDto> save(DepartmentDto departmentDto) throws DepartmentException{
        try {
            Department department = modelMapper.map(departmentDto, Department.class);
            departmentRepository.save(department);
        } catch (Exception e){
            throw new DepartmentException(String.format("Error when create Department: %s", e.getMessage()), e);
        }
        return Optional.of(departmentDto);
    }

    @Override
    public Optional<DepartmentDto> findByDeptNo(Long deptNo) {
        return departmentRepository.findByDeptNo(deptNo).map(department -> modelMapper.map(department, DepartmentDto.class));
    }

    @Override
    public Optional<DepartmentDto> update(Long deptNo, DepartmentDto departmentDto) {
        try {
            Optional<Department> departmentOptional = Optional.of(departmentRepository.getById(deptNo));
            departmentOptional.map(department -> {
                        Department dept = modelMapper.map(departmentDto, Department.class);
                        dept.setDeptNo(department.getDeptNo());
                        departmentRepository.save(dept);
                        return dept;
                    })
                    .orElseThrow(() -> new DepartmentException(DepartmentConstant.DEPARTMENT_NOT_FOUND));
        } catch (Exception e){
            throw new DepartmentException(String.format("Error when update Department: %s", e.getMessage()), e);
        }
        return Optional.of(departmentDto);
    }

    @Override
    public Optional<DepartmentDto> delete(Long deptNo, DepartmentDto departmentDto) {
        try {
            Optional<Department> departmentOptional = Optional.of(departmentRepository.getById(deptNo));
            departmentOptional.ifPresent(department -> departmentRepository.delete(department));
        } catch (Exception e){
            throw new DepartmentException(String.format("Error when delete Employee: %s", e.getMessage()), e);
        }
        return Optional.of(departmentDto);
    }

    @Override
    public Optional<DepartmentDto> deleteById(Long deptNo) {
        Optional<DepartmentDto> departmentDto;
        try {
            Optional<Department> departmentOptional = departmentRepository.findByDeptNo(deptNo);
            if(departmentOptional.isPresent()) {
                departmentRepository.deleteById(departmentOptional.get().getDeptNo());
                departmentDto = departmentOptional.map(department -> modelMapper.map(department, DepartmentDto.class));
            } else {
                throw new DepartmentException(DepartmentConstant.DEPARTMENT_NOT_FOUND);
            }
        } catch (Exception e){
            throw new DepartmentException(String.format("Error when delete Employee: %s", e.getMessage()), e);
        }
        return departmentDto;
    }

    @Override
    public List<DepartmentDto> findAll() {
        return departmentRepository.findAll().stream()
                .map(department -> modelMapper.map(department, DepartmentDto.class))
                .collect(Collectors.toList());
    }
}
