package com.astralife.emp.service.impl;

import com.astralife.emp.constant.DepartmentEmployeeConstant;
import com.astralife.emp.dto.DepartmentEmployeeDto;
import com.astralife.emp.exception.DepartmentEmployeeException;
import com.astralife.emp.exception.DepartmentException;
import com.astralife.emp.model.DepartmentEmployee;
import com.astralife.emp.repository.DepartmentEmployeeRepository;
import com.astralife.emp.service.DepartmentEmployeeService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class DepartmentEmployeeServiceImpl implements DepartmentEmployeeService {
    private final ModelMapper modelMapper;
    
    private final DepartmentEmployeeRepository departmentEmployeeRepository;

    @Autowired
    public DepartmentEmployeeServiceImpl(ModelMapper modelMapper, DepartmentEmployeeRepository departmentEmployeeRepository) {
        this.modelMapper = modelMapper;
        this.departmentEmployeeRepository = departmentEmployeeRepository;
    }

    @Override
    public Optional<DepartmentEmployeeDto> save(DepartmentEmployeeDto departmentEmployeeDto) {
        try {
            DepartmentEmployee departmentEmployee = modelMapper.map(departmentEmployeeDto, DepartmentEmployee.class);
            departmentEmployeeRepository.save(departmentEmployee);
        } catch (Exception e){
            throw new DepartmentEmployeeException(String.format("Error when create Department Employee: %s", e.getMessage()), e);
        }
        return Optional.of(departmentEmployeeDto);
    }

    @Override
    public List<DepartmentEmployeeDto> findByDeptNo(Long deptNo) {
        return departmentEmployeeRepository.findByDeptNo(deptNo).stream()
                .map(departmentEmployee -> modelMapper.map(departmentEmployee, DepartmentEmployeeDto.class)).collect(Collectors.toList());
    }

    @Override
    public Optional<DepartmentEmployeeDto> findByDeptNoAndEmpNo(Long deptNo, Long empNo) {
        return departmentEmployeeRepository.findByDeptNoAndEmpNo(deptNo, empNo).map(departmentEmployee -> modelMapper.map(departmentEmployee, DepartmentEmployeeDto.class));
    }

    @Override
    public Optional<DepartmentEmployeeDto> update(DepartmentEmployeeDto departmentEmployeeDto) {
        try {
            departmentEmployeeRepository.findByDeptNoAndEmpNo(departmentEmployeeDto.getDeptNo(), departmentEmployeeDto.getEmpNo())
                    .map(departmentEmployee ->  departmentEmployeeRepository.save(modelMapper.map(departmentEmployeeDto, DepartmentEmployee.class)))
                    .orElseThrow(() -> new DepartmentEmployeeException(DepartmentEmployeeConstant.DEPARTMENT_EMPLOYEE_NOT_FOUND));
        } catch (Exception e){
            throw new DepartmentException(String.format("Error when update Department Employee: %s", e.getMessage()), e);
        }
        return Optional.of(departmentEmployeeDto);
    }

    @Override
    public Optional<DepartmentEmployeeDto> delete(DepartmentEmployeeDto departmentEmployeeDto) {
        try{
            Optional<DepartmentEmployee> departmentEmployeeOptional = departmentEmployeeRepository.findByDeptNoAndEmpNo(departmentEmployeeDto.getDeptNo(), departmentEmployeeDto.getEmpNo());
            if(departmentEmployeeOptional.isPresent()){
                departmentEmployeeRepository.delete(departmentEmployeeOptional.get());
            } else {
                throw new DepartmentEmployeeException(DepartmentEmployeeConstant.DEPARTMENT_EMPLOYEE_NOT_FOUND);
            }
        } catch (Exception e){
            throw new DepartmentEmployeeException(String.format("Error when delete Department Employee: %s", e.getMessage()), e);
        }
        return Optional.of(departmentEmployeeDto);
    }

    @Override
    public List<DepartmentEmployeeDto> findAll() {
        return departmentEmployeeRepository.findAll().stream()
                .map(departmentEmployee -> modelMapper.map(departmentEmployee, DepartmentEmployeeDto.class)).collect(Collectors.toList());
    }
}
