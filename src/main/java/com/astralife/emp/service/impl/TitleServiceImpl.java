package com.astralife.emp.service.impl;

import com.astralife.emp.constant.TitleConstant;
import com.astralife.emp.dto.TitleDto;
import com.astralife.emp.exception.TitleException;
import com.astralife.emp.model.Title;
import com.astralife.emp.repository.TitleRepository;
import com.astralife.emp.service.TitleService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class TitleServiceImpl implements TitleService {
    private final ModelMapper modelMapper;

    private final TitleRepository titleRepository;

    @Autowired
    public TitleServiceImpl(ModelMapper modelMapper, TitleRepository titleRepository) {
        this.modelMapper = modelMapper;
        this.titleRepository = titleRepository;
    }

    @Override
    public Optional<TitleDto> save(TitleDto titleDto) {
        try{
            Title title = modelMapper.map(titleDto, Title.class);
            titleRepository.save(title);
        } catch (Exception e){
            throw new TitleException(String.format("Error when create Title: %s", e.getMessage()), e);
        }
        return Optional.of(titleDto);
    }

    @Override
    public Optional<TitleDto> findByEmpNoAndFromDateAndTitle(Long empNo, LocalDate fromDate, String title) {
        return titleRepository.findByEmpNoAndFromDateAndTitle(empNo,fromDate,title).map(titleEn -> modelMapper.map(titleEn, TitleDto.class));
    }

    @Override
    public Optional<TitleDto> update(TitleDto titleDto) {
        try {
            titleRepository.findByEmpNoAndFromDateAndTitle(titleDto.getEmpNo(), titleDto.getFromDate(), titleDto.getTitle())
                    .map(title -> titleRepository.save(modelMapper.map(titleDto, Title.class))).orElseThrow(() -> new TitleException(TitleConstant.TITLE_NOT_FOUND));
        } catch (Exception e){
            throw new TitleException(String.format("Error when update Title: %s", e.getMessage()), e);
        }
        return Optional.of(titleDto);
    }

    @Override
    public List<TitleDto> findAll() {
        return titleRepository.findAll().stream().map(title -> modelMapper.map(title, TitleDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public Optional<TitleDto> delete(TitleDto titleDto) {
        try{
            Optional<Title> titleOptional = titleRepository.findByEmpNoAndFromDateAndTitle(titleDto.getEmpNo(), titleDto.getFromDate(), titleDto.getTitle());
            if(titleOptional.isPresent()){
                titleRepository.delete(titleOptional.get());
            } else {
                throw new TitleException(TitleConstant.TITLE_NOT_FOUND);
            }
        } catch (Exception e){
            throw new TitleException(String.format("Error when delete Title: %s", e.getMessage()), e);
        }
        return Optional.of(titleDto);
    }
}
