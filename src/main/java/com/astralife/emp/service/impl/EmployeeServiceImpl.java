package com.astralife.emp.service.impl;

import com.astralife.emp.constant.EmployeeConstant;
import com.astralife.emp.dto.EmployeeDto;
import com.astralife.emp.exception.EmployeeException;
import org.modelmapper.ModelMapper;
import com.astralife.emp.model.Employee;
import com.astralife.emp.repository.EmployeeRepository;
import com.astralife.emp.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService {
    private final ModelMapper modelMapper;

    private final EmployeeRepository employeeRepository;

    @Autowired
    public EmployeeServiceImpl(ModelMapper modelMapper, EmployeeRepository employeeRepository) {
        this.modelMapper = modelMapper;
        this.employeeRepository = employeeRepository;
    }

    @Override
    public Optional<EmployeeDto> findByEmpNo(Long empNo) {
        return employeeRepository.findByEmpNo(empNo).map(employee -> modelMapper.map(employee, EmployeeDto.class));
    }

    @Override
    public Optional<EmployeeDto> save(EmployeeDto employeeDto) {
        try {
            Employee emp = modelMapper.map(employeeDto, Employee.class);
            employeeRepository.save(emp);
        } catch (Exception e) {
            throw new EmployeeException(String.format("Error when create Employee: %s", e.getMessage()), e);
        }
        return Optional.of(employeeDto);
    }

    @Override
    public Optional<EmployeeDto> update(Long empNo, EmployeeDto employeeDto) throws EmployeeException {
        try {
            Optional<Employee> employeeOptional = Optional.of(employeeRepository.getById(empNo));
            employeeOptional.map(employee -> {
                        Employee emp = modelMapper.map(employeeDto, Employee.class);
                        emp.setEmpNo(employee.getEmpNo());
                        employeeRepository.save(emp);
                        return emp;
                    })
                    .orElseThrow(() -> new EmployeeException(EmployeeConstant.EMPLOYEE_NOT_FOUND));
        } catch (Exception e){
            throw new EmployeeException(String.format("Error when update Employee: %s", e.getMessage()), e);
        }
        return Optional.of(employeeDto);
    }

    public List<EmployeeDto> findAll() {
        return employeeRepository.findAll().stream()
                .map(employee -> modelMapper.map(employee, EmployeeDto.class))
                .collect(Collectors.toList());
    }

    public Optional<EmployeeDto> delete(Long empNo, EmployeeDto employeeDto) throws EmployeeException{
        try {
            Optional<Employee> employee = Optional.of(employeeRepository.getById(empNo));
            employee.ifPresent(emp -> employeeRepository.delete(emp));
        } catch (Exception e){
            throw new EmployeeException(String.format("Error when delete Employee: %s", e.getMessage()), e);
        }
        return Optional.of(employeeDto);
    }

    public Optional<EmployeeDto> deleteById(Long empNo) {
        Optional<EmployeeDto> employeeDto;
        try {
            Optional<Employee> employeeOptional = employeeRepository.findByEmpNo(empNo);
            if (employeeOptional.isPresent()){
                employeeRepository.deleteById(employeeOptional.get().getEmpNo());
                employeeDto = employeeOptional.map(employee -> modelMapper.map(employee, EmployeeDto.class));
            } else {
                throw new EmployeeException(EmployeeConstant.EMPLOYEE_NOT_FOUND);
            }
        } catch (Exception e){
            throw new EmployeeException(String.format("Error when delete Employee: %s", e.getMessage()), e);
        }
        return employeeDto;
    }
}