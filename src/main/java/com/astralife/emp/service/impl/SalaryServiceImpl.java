package com.astralife.emp.service.impl;

import com.astralife.emp.constant.EmployeeConstant;
import com.astralife.emp.constant.SalaryConstant;
import com.astralife.emp.dto.SalaryDto;
import com.astralife.emp.exception.SalaryException;
import com.astralife.emp.model.Salary;
import com.astralife.emp.repository.SalaryRepository;
import com.astralife.emp.service.SalaryService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class SalaryServiceImpl implements SalaryService {
    private final ModelMapper modelMapper;

    private final SalaryRepository salaryRepository;

    @Autowired
    public SalaryServiceImpl(ModelMapper modelMapper, SalaryRepository salaryRepository) {
        this.modelMapper = modelMapper;
        this.salaryRepository = salaryRepository;
    }

    @Override
    public Optional<SalaryDto> save(SalaryDto salaryDto) {
        try {
            Salary salary = modelMapper.map(salaryDto, Salary.class);
            salaryRepository.save(salary);
        } catch (Exception e){
            throw new SalaryException(String.format("Error when create Salary: %s", e.getMessage()), e);
        }
        return Optional.of(salaryDto);
    }

    @Override
    public Optional<SalaryDto> findByEmpNoAndFromDate(Long empNo, LocalDate fromDate) {
        return salaryRepository.findByEmpNoAndFromDate(empNo, fromDate)
                .map(salary -> modelMapper.map(salary, SalaryDto.class));
    }

    @Override
    public Optional<SalaryDto> update(SalaryDto salaryDto) {
        try {
                salaryRepository.findByEmpNoAndFromDate(salaryDto.getEmpNo(), salaryDto.getFromDate())
                        .map(salary -> salaryRepository.save(modelMapper.map(salaryDto, Salary.class)))
                        .orElseThrow(() -> new SalaryException(EmployeeConstant.EMPLOYEE_NOT_FOUND));
        } catch (Exception e){
            throw new SalaryException(String.format("Error when update Salary: %s", e.getMessage()), e);
        }
        return Optional.of(salaryDto);
    }

    @Override
    public Optional<SalaryDto> delete(SalaryDto salaryDto) {
        try {
            Optional<Salary> salaryOptional = salaryRepository.findByEmpNoAndFromDate(salaryDto.getEmpNo(), salaryDto.getFromDate());
            if(salaryOptional.isPresent()){
                salaryRepository.delete(salaryOptional.get());
            } else {
                throw new SalaryException(SalaryConstant.SALARY_NOT_FOUND);
            }
        } catch (Exception e){
            throw new SalaryException(String.format("Error when delete Salary: %s", e.getMessage()), e);
        }
        return Optional.of(salaryDto);
    }

    @Override
    public List<SalaryDto> findAll() {
        return salaryRepository.findAll().stream()
                .map(salary -> modelMapper.map(salary, SalaryDto.class)).collect(Collectors.toList());
    }
}