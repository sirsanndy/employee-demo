package com.astralife.emp.service.impl;

import com.astralife.emp.constant.DepartmentManagerConstant;
import com.astralife.emp.constant.TitleConstant;
import com.astralife.emp.dto.DepartmentManagerDto;
import com.astralife.emp.exception.DepartmentException;
import com.astralife.emp.exception.DepartmentManagerException;
import com.astralife.emp.model.DepartmentManager;
import com.astralife.emp.repository.DepartmentManagerRepository;
import com.astralife.emp.service.DepartmentManagerService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class DepartmentManagerServiceImpl implements DepartmentManagerService {
    private final ModelMapper modelMapper;

    private final DepartmentManagerRepository departmentManagerRepository;

    @Autowired
    public DepartmentManagerServiceImpl(ModelMapper modelMapper, DepartmentManagerRepository departmentManagerRepository) {
        this.modelMapper = modelMapper;
        this.departmentManagerRepository = departmentManagerRepository;
    }

    @Override
    public Optional<DepartmentManagerDto> save(DepartmentManagerDto departmentManagerDto) {
        try {
            DepartmentManager departmentManager = modelMapper.map(departmentManagerDto, DepartmentManager.class);
            departmentManagerRepository.save(departmentManager);
        } catch (Exception e){
            throw new DepartmentManagerException(String.format("Error when create Department Manager: %s", e.getMessage()), e);
        }
        return Optional.of(departmentManagerDto);
    }

    @Override
    public List<DepartmentManagerDto> findByDeptNo(Long deptNo) {
        return departmentManagerRepository.findByDeptNo(deptNo).stream()
                .map(departmentManager -> modelMapper.map(departmentManager, DepartmentManagerDto.class)).collect(Collectors.toList());
    }

    @Override
    public Optional<DepartmentManagerDto> findByEmpNoAndDeptNo(Long empNo, Long deptNo) {
        return departmentManagerRepository.findByEmpNoAndDeptNo(empNo, deptNo).map(departmentManager -> modelMapper.map(departmentManager, DepartmentManagerDto.class));
    }


    @Override
    public Optional<DepartmentManagerDto> update(DepartmentManagerDto departmentManagerDto) {
        try {
            departmentManagerRepository.findByEmpNoAndDeptNo(departmentManagerDto.getEmpNo(), departmentManagerDto.getDeptNo())
                    .map(departmentManager ->  departmentManagerRepository.save(modelMapper.map(departmentManagerDto, DepartmentManager.class)))
                    .orElseThrow(() -> new DepartmentManagerException(DepartmentManagerConstant.DEPARTMENT_MANAGER_NOT_FOUND));
        } catch (Exception e){
            throw new DepartmentException(String.format("Error when update Department Manager: %s", e.getMessage()), e);
        }
        return Optional.of(departmentManagerDto);
    }

    @Override
    public Optional<DepartmentManagerDto> delete(DepartmentManagerDto departmentManagerDto) {
        try{
            Optional<DepartmentManager> departmentManagerOptional = departmentManagerRepository.findByEmpNoAndDeptNo(departmentManagerDto.getEmpNo(), departmentManagerDto.getDeptNo());
            if(departmentManagerOptional.isPresent()){
                departmentManagerRepository.delete(departmentManagerOptional.get());
            } else {
                throw new DepartmentManagerException(TitleConstant.TITLE_NOT_FOUND);
            }
        } catch (Exception e){
            throw new DepartmentManagerException(String.format("Error when delete Department Manager: %s", e.getMessage()), e);
        }
        return Optional.of(departmentManagerDto);
    }

    @Override
    public List<DepartmentManagerDto> findAll() {
        return departmentManagerRepository.findAll().stream()
                .map(departmentManager -> modelMapper.map(departmentManager, DepartmentManagerDto.class)).collect(Collectors.toList());
    }
}
