package com.astralife.emp.service;

import com.astralife.emp.dto.DepartmentDto;
import com.astralife.emp.exception.DepartmentException;

import java.util.List;
import java.util.Optional;

public interface DepartmentService {
    Optional<DepartmentDto> save(DepartmentDto departmentDto) throws DepartmentException;

    Optional<DepartmentDto> findByDeptNo(Long deptNo);

    Optional<DepartmentDto>  update(Long deptNo, DepartmentDto employeeDto);

    Optional<DepartmentDto> delete(Long deptNo, DepartmentDto departmentDto);

    Optional<DepartmentDto>  deleteById(Long deptNo);

    List<DepartmentDto> findAll();
}
