package com.astralife.emp.service;

import com.astralife.emp.dto.DepartmentEmployeeDto;

import java.util.List;
import java.util.Optional;

public interface DepartmentEmployeeService {

    Optional<DepartmentEmployeeDto> save(DepartmentEmployeeDto departmentEmployeeDto);

    List<DepartmentEmployeeDto> findByDeptNo(Long deptNo);

    Optional<DepartmentEmployeeDto> findByDeptNoAndEmpNo(Long deptNo, Long empNo);

    Optional<DepartmentEmployeeDto> update(DepartmentEmployeeDto departmentManagerDto);

    Optional<DepartmentEmployeeDto> delete(DepartmentEmployeeDto departmentManagerDto);

    List<DepartmentEmployeeDto> findAll();
}
