package com.astralife.emp.controller;

import com.astralife.emp.constant.EmployeeConstant;
import com.astralife.emp.constant.SalaryConstant;
import com.astralife.emp.dto.SalaryDto;
import com.astralife.emp.exception.EmployeeException;
import com.astralife.emp.exception.SalaryException;
import com.astralife.emp.service.EmployeeService;
import com.astralife.emp.service.SalaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.Optional;

@RestController
@RequestMapping("/api/salary")
public class SalaryController {
    private final SalaryService salaryService;
    private final EmployeeService employeeService;

    @Autowired
    public SalaryController(SalaryService salaryService, EmployeeService employeeService) {
        this.salaryService = salaryService;
        this.employeeService = employeeService;
    }

    @GetMapping
    public Iterable<SalaryDto> findAll(){
        return salaryService.findAll();
    }

    @GetMapping("/{empNo}/{fromDate}")
    public ResponseEntity<Object> findByEmpNoAndFromDate(@PathVariable Long empNo, @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate fromDate){
        Optional<SalaryDto> departmentDtoOptional = salaryService.findByEmpNoAndFromDate(empNo, fromDate);
        return departmentDtoOptional.<ResponseEntity<Object>>map(departmentDto -> new ResponseEntity<>(departmentDto, HttpStatus.FOUND))
                .orElseGet(() -> new ResponseEntity<>("Salary with Emp No : " + empNo + " is not found", HttpStatus.NOT_FOUND));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Object> create(@RequestBody SalaryDto salaryDto) {
        if(salaryService.findByEmpNoAndFromDate(salaryDto.getEmpNo(), salaryDto.getFromDate()).isEmpty()) {
            employeeService.findByEmpNo(salaryDto.getEmpNo()).map(employeeDto -> salaryService.save(salaryDto))
                            .orElseThrow(() -> new EmployeeException(EmployeeConstant.EMPLOYEE_NOT_FOUND));
        } else {
            return new ResponseEntity<>("Salary with Emp No : " + salaryDto.getEmpNo() + " is already exist", HttpStatus.CONFLICT);
        }
        return new ResponseEntity<>(salaryDto, HttpStatus.CREATED);
    }

    @DeleteMapping("/{empNo}/{fromDate}")
    public void delete(@PathVariable Long empNo, @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate fromDate) {
        salaryService.findByEmpNoAndFromDate(empNo,fromDate).map(salaryService::delete)
                .orElseThrow(() -> new SalaryException(SalaryConstant.SALARY_NOT_FOUND));
    }

    @PutMapping("/{empNo}/{fromDate}")
    public SalaryDto update(@RequestBody SalaryDto salaryDto, @PathVariable Long empNo, @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate fromDate) {
        salaryService.findByEmpNoAndFromDate(empNo, fromDate).map(s -> salaryService.update(salaryDto))
                .orElseThrow(() -> new SalaryException(SalaryConstant.SALARY_NOT_FOUND));
        return salaryDto;
    }
}
