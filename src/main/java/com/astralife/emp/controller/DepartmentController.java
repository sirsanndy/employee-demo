package com.astralife.emp.controller;

import com.astralife.emp.constant.DepartmentConstant;
import com.astralife.emp.dto.DepartmentDto;
import com.astralife.emp.exception.DepartmentException;
import com.astralife.emp.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/department")
public class DepartmentController {
    private final DepartmentService departmentService;

    @Autowired
    public DepartmentController(DepartmentService departmentService) {
        this.departmentService = departmentService;
    }

    @GetMapping
    public Iterable<DepartmentDto> findAll(){
        return departmentService.findAll();
    }

    @GetMapping("/{deptNo}")
    public ResponseEntity<Object> findByDeptNo(@PathVariable Long deptNo){
        Optional<DepartmentDto> departmentDtoOptional = departmentService.findByDeptNo(deptNo);
        return departmentDtoOptional.<ResponseEntity<Object>>map(departmentDto -> new ResponseEntity<>(departmentDto, HttpStatus.FOUND))
                .orElseGet(() -> new ResponseEntity<>("Department with Dept No : " + deptNo + " is not found", HttpStatus.NOT_FOUND));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Object> create(@RequestBody DepartmentDto departmentDto) {
        departmentService.save(departmentDto);
        return new ResponseEntity<>(departmentDto, HttpStatus.CREATED);
    }

    @DeleteMapping("/{deptNo}")
    public void delete(@PathVariable Long deptNo) {
        departmentService.findByDeptNo(deptNo).map(departmentDto -> departmentService.delete(deptNo, departmentDto))
                .orElseThrow(() -> new DepartmentException(DepartmentConstant.DEPARTMENT_NOT_FOUND));
    }

    @PutMapping("/{deptNo}")
    public DepartmentDto update(@RequestBody DepartmentDto departmentDto, @PathVariable Long deptNo) {
        departmentService.findByDeptNo(deptNo).map(d -> departmentService.update(deptNo, departmentDto))
                .orElseThrow(() -> new DepartmentException(DepartmentConstant.DEPARTMENT_NOT_FOUND));
        return departmentDto;
    }
}
