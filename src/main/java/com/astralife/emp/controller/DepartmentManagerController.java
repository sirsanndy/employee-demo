package com.astralife.emp.controller;

import com.astralife.emp.constant.DepartmentConstant;
import com.astralife.emp.constant.DepartmentManagerConstant;
import com.astralife.emp.constant.EmployeeConstant;
import com.astralife.emp.dto.DepartmentManagerDto;
import com.astralife.emp.exception.DepartmentException;
import com.astralife.emp.exception.DepartmentManagerException;
import com.astralife.emp.exception.EmployeeException;
import com.astralife.emp.service.DepartmentManagerService;
import com.astralife.emp.service.DepartmentService;
import com.astralife.emp.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/deptmgr")
public class DepartmentManagerController {
    private final DepartmentManagerService departmentManagerService;
    private final EmployeeService employeeService;
    private final DepartmentService departmentService;

    @Autowired
    public DepartmentManagerController(DepartmentManagerService departmentManagerService, EmployeeService employeeService,
                                       DepartmentService departmentService) {
        this.departmentManagerService = departmentManagerService;
        this.employeeService = employeeService;
        this.departmentService = departmentService;
    }

    @GetMapping
    public Iterable<DepartmentManagerDto> findAll() {
        return departmentManagerService.findAll();
    }

    @GetMapping("/{empNo}/{deptNo}")
    public ResponseEntity<Object> findByEmpNoAndDeptNo(@PathVariable Long empNo, @PathVariable Long deptNo) {
        Optional<DepartmentManagerDto> departmentManagerDtoOptional = departmentManagerService.findByEmpNoAndDeptNo(empNo, deptNo);
        return departmentManagerDtoOptional.<ResponseEntity<Object>>map(departmentManagerDto -> new ResponseEntity<>(departmentManagerDto, HttpStatus.FOUND))
                .orElseGet(() -> new ResponseEntity<>("Department Manager with Emp No : " + empNo + " is not found", HttpStatus.NOT_FOUND));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Object> create(@RequestBody DepartmentManagerDto departmentManagerDto) {
        if (departmentManagerService.findByEmpNoAndDeptNo(departmentManagerDto.getEmpNo(), departmentManagerDto.getDeptNo()).isEmpty()) {
            employeeService.findByEmpNo(departmentManagerDto.getEmpNo()).orElseThrow(() -> new EmployeeException(EmployeeConstant.EMPLOYEE_NOT_FOUND));
            departmentService.findByDeptNo(departmentManagerDto.getDeptNo()).map(departmentDto -> departmentManagerService.save(departmentManagerDto))
                    .orElseThrow(() -> new DepartmentException(DepartmentConstant.DEPARTMENT_NOT_FOUND));
        } else {
            return new ResponseEntity<>("Department Manager with Emp No : " + departmentManagerDto.getEmpNo() + " is already exist", HttpStatus.CONFLICT);
        }
        return new ResponseEntity<>(departmentManagerDto, HttpStatus.CREATED);
    }

    @DeleteMapping("/{empNo}/{deptNo}")
    public void delete(@PathVariable Long empNo, @PathVariable Long deptNo) {
        departmentManagerService.findByEmpNoAndDeptNo(empNo, deptNo).map(departmentManagerService::delete)
                .orElseThrow(() -> new DepartmentManagerException(DepartmentManagerConstant.DEPARTMENT_MANAGER_NOT_FOUND));
    }

    @PutMapping("/{empNo}/{deptNo}")
    public DepartmentManagerDto update(@RequestBody DepartmentManagerDto departmentManagerDto, @PathVariable Long empNo, @PathVariable Long deptNo) {
        departmentManagerService.findByEmpNoAndDeptNo(empNo, deptNo).map(d -> departmentManagerService.update(departmentManagerDto))
                .orElseThrow(() -> new DepartmentManagerException(DepartmentManagerConstant.DEPARTMENT_MANAGER_NOT_FOUND));
        return departmentManagerDto;
    }
}
