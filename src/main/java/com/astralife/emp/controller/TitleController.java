package com.astralife.emp.controller;

import com.astralife.emp.constant.EmployeeConstant;
import com.astralife.emp.constant.TitleConstant;
import com.astralife.emp.dto.TitleDto;
import com.astralife.emp.exception.EmployeeException;
import com.astralife.emp.exception.TitleException;
import com.astralife.emp.service.EmployeeService;
import com.astralife.emp.service.TitleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.Optional;

@RestController
@RequestMapping("/api/title")
public class TitleController {
    private final TitleService titleService;
    private final EmployeeService employeeService;

    @Autowired
    public TitleController(TitleService titleService, EmployeeService employeeService) {
        this.titleService = titleService;
        this.employeeService = employeeService;
    }

    @GetMapping
    public Iterable<TitleDto> findAll(){
        return titleService.findAll();
    }

    @GetMapping("/{empNo}/{fromDate}/{title}")
    public ResponseEntity<Object> findByEmpNoAndFromDateAndTitle(@PathVariable Long empNo, @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate fromDate
    , @PathVariable String title){
        Optional<TitleDto> departmentDtoOptional = titleService.findByEmpNoAndFromDateAndTitle(empNo, fromDate, title);
        return departmentDtoOptional.<ResponseEntity<Object>>map(departmentDto -> new ResponseEntity<>(departmentDto, HttpStatus.FOUND))
                .orElseGet(() -> new ResponseEntity<>("Title with Emp No : " + empNo + " is not found", HttpStatus.NOT_FOUND));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Object> create(@RequestBody TitleDto titleDto) {
        if(titleService.findByEmpNoAndFromDateAndTitle(titleDto.getEmpNo(), titleDto.getFromDate(), titleDto.getTitle()).isEmpty()) {
            employeeService.findByEmpNo(titleDto.getEmpNo()).map(employeeDto -> titleService.save(titleDto))
                    .orElseThrow(() -> new EmployeeException(EmployeeConstant.EMPLOYEE_NOT_FOUND));
        } else {
            return new ResponseEntity<>("Title with Emp No : " + titleDto.getEmpNo() + " is already exist", HttpStatus.CONFLICT);
        }
        return new ResponseEntity<>(titleDto, HttpStatus.CREATED);
    }

    @DeleteMapping("/{empNo}/{fromDate}/{title}")
    public void delete(@PathVariable Long empNo, @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate fromDate,
                       @PathVariable String title) {
        titleService.findByEmpNoAndFromDateAndTitle(empNo, fromDate, title).map(titleService::delete)
                .orElseThrow(() -> new TitleException(TitleConstant.TITLE_NOT_FOUND));
    }

    @PutMapping("/{empNo}/{fromDate}/{title}")
    public TitleDto update(@RequestBody TitleDto titleDto, @PathVariable Long empNo, @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate fromDate,
                           @PathVariable String title) {
        titleService.findByEmpNoAndFromDateAndTitle(empNo, fromDate, title).map(t -> titleService.update(titleDto))
                .orElseThrow(() -> new TitleException(TitleConstant.TITLE_NOT_FOUND));
        return titleDto;
    }
}
