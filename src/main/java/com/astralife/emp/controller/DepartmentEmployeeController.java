package com.astralife.emp.controller;

import com.astralife.emp.constant.DepartmentConstant;
import com.astralife.emp.constant.DepartmentEmployeeConstant;
import com.astralife.emp.constant.EmployeeConstant;
import com.astralife.emp.dto.DepartmentEmployeeDto;
import com.astralife.emp.exception.DepartmentException;
import com.astralife.emp.exception.DepartmentEmployeeException;
import com.astralife.emp.exception.EmployeeException;
import com.astralife.emp.service.DepartmentEmployeeService;
import com.astralife.emp.service.DepartmentService;
import com.astralife.emp.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/deptemp")
public class DepartmentEmployeeController {
    private final DepartmentEmployeeService departmentEmployeeService;
    private final DepartmentService departmentService;
    private final EmployeeService employeeService;

    @Autowired
    public DepartmentEmployeeController(DepartmentEmployeeService departmentEmployeeService,
                                        DepartmentService departmentService, EmployeeService employeeService) {
        this.departmentEmployeeService = departmentEmployeeService;
        this.departmentService = departmentService;
        this.employeeService = employeeService;
    }

    @GetMapping
    public Iterable<DepartmentEmployeeDto> findAll() {
        return departmentEmployeeService.findAll();
    }

    @GetMapping("/{deptNo}/{empNo}")
    public ResponseEntity<Object> findByDeptNoAndEmpNo(@PathVariable Long empNo, @PathVariable Long deptNo) {
        Optional<DepartmentEmployeeDto> departmentEmployeeDtoOptional = departmentEmployeeService.findByDeptNoAndEmpNo(deptNo, empNo);
        return departmentEmployeeDtoOptional.<ResponseEntity<Object>>map(departmentEmployeeDto -> new ResponseEntity<>(departmentEmployeeDto, HttpStatus.FOUND))
                .orElseGet(() -> new ResponseEntity<>("Department Employee with Emp No : " + empNo + " is not found", HttpStatus.NOT_FOUND));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Object> create(@RequestBody DepartmentEmployeeDto departmentEmployeeDto) {
        if (departmentEmployeeService.findByDeptNoAndEmpNo(departmentEmployeeDto.getEmpNo(), departmentEmployeeDto.getDeptNo()).isEmpty()) {
            departmentService.findByDeptNo(departmentEmployeeDto.getDeptNo())
                    .orElseThrow(() -> new DepartmentException(DepartmentConstant.DEPARTMENT_NOT_FOUND));
            employeeService.findByEmpNo(departmentEmployeeDto.getEmpNo()).map(departmentDto -> departmentEmployeeService.save(departmentEmployeeDto))
                    .orElseThrow(() -> new EmployeeException(EmployeeConstant.EMPLOYEE_NOT_FOUND));
        } else {
            return new ResponseEntity<>("Department Employee with Emp No : " + departmentEmployeeDto.getEmpNo() + " is already exist", HttpStatus.CONFLICT);
        }
        return new ResponseEntity<>(departmentEmployeeDto, HttpStatus.CREATED);
    }

    @DeleteMapping("/{deptNo}/{empNo}")
    public void delete(@PathVariable Long deptNo, @PathVariable Long empNo) {
        departmentEmployeeService.findByDeptNoAndEmpNo(deptNo, empNo).map(departmentEmployeeService::delete)
                .orElseThrow(() -> new DepartmentEmployeeException(DepartmentEmployeeConstant.DEPARTMENT_EMPLOYEE_NOT_FOUND));
    }

    @PutMapping("/{deptNo}/{empNo}")
    public DepartmentEmployeeDto update(@RequestBody DepartmentEmployeeDto departmentEmployeeDto, @PathVariable Long deptNo, @PathVariable Long empNo) {
        departmentEmployeeService.findByDeptNoAndEmpNo(deptNo, empNo).map(d -> departmentEmployeeService.update(departmentEmployeeDto))
                .orElseThrow(() -> new DepartmentEmployeeException(DepartmentEmployeeConstant.DEPARTMENT_EMPLOYEE_NOT_FOUND));
        return departmentEmployeeDto;
    }
}
