package com.astralife.emp.controller;

import com.astralife.emp.constant.EmployeeConstant;
import com.astralife.emp.dto.EmployeeDto;
import com.astralife.emp.exception.EmployeeException;
import com.astralife.emp.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/employee")
public class EmployeeController {

    private final EmployeeService employeeService;

    @Autowired
    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping
    public Iterable<EmployeeDto> findAll(){
        return employeeService.findAll();
    }

    @GetMapping("/{empNo}")
    public ResponseEntity<Object> findByEmpNo(@PathVariable Long empNo) {
        Optional<EmployeeDto> employeeDtoOptional = employeeService.findByEmpNo(empNo);
        return employeeDtoOptional.<ResponseEntity<Object>>map(employeeDto -> new ResponseEntity<>(employeeDto, HttpStatus.FOUND)).orElseGet(() -> new ResponseEntity<>("Employee with Emp No : " + empNo + " is not found", HttpStatus.NOT_FOUND));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Object> create(@RequestBody EmployeeDto employeeDto) throws EmployeeException{
        employeeService.save(employeeDto);
        return new ResponseEntity<>(employeeDto, HttpStatus.CREATED);
    }

    @DeleteMapping("/{empNo}")
    public void delete(@PathVariable Long empNo) {
        employeeService.findByEmpNo(empNo).map(employeeDto -> employeeService.delete(empNo, employeeDto))
                .orElseThrow(() -> new EmployeeException(EmployeeConstant.EMPLOYEE_NOT_FOUND));
    }

    @PutMapping("/{empNo}")
    public EmployeeDto update(@RequestBody EmployeeDto employeeDto, @PathVariable Long empNo) {
        employeeService.findByEmpNo(empNo).map(empDto -> employeeService.update(empNo, employeeDto))
                .orElseThrow(() -> new EmployeeException(EmployeeConstant.EMPLOYEE_NOT_FOUND));
        return employeeDto;
    }
}
