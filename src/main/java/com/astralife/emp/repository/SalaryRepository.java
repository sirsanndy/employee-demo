package com.astralife.emp.repository;

import com.astralife.emp.model.Salary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Optional;

@Repository
public interface SalaryRepository extends JpaRepository<Salary, Long> {

    Optional<Salary> findByEmpNoAndFromDate (Long empNo, LocalDate fromDate);
}