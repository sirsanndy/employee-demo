package com.astralife.emp.repository;

import com.astralife.emp.model.DepartmentManager;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DepartmentManagerRepository extends JpaRepository<DepartmentManager, Long> {
    List<DepartmentManager> findByEmpNo(Long empNo);

    List<DepartmentManager> findByDeptNo(Long deptNo);

    Optional<DepartmentManager> findByEmpNoAndDeptNo(Long empNo, Long deptNo);
}
