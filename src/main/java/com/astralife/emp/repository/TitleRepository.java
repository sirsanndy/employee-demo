package com.astralife.emp.repository;

import com.astralife.emp.model.Title;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface TitleRepository extends JpaRepository<Title, Long> {
    List<Title> findByEmpNo(Long empNo);

    Optional<Title> findByEmpNoAndFromDateAndTitle(Long empNo, LocalDate fromDate, String title);
}
