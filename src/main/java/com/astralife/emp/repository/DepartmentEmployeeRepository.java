package com.astralife.emp.repository;

import com.astralife.emp.model.DepartmentEmployee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DepartmentEmployeeRepository extends JpaRepository<DepartmentEmployee, Long> {
    List<DepartmentEmployee> findByEmpNo(Long empNo);

    List<DepartmentEmployee> findByDeptNo(Long deptNo);

    Optional<DepartmentEmployee> findByDeptNoAndEmpNo(Long deptNo, Long empNo);
}
