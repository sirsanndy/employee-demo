package com.astralife.emp.constant;

public class DepartmentConstant {
    public static final String DEPARTMENT_NOT_FOUND = "Department not found";

    public DepartmentConstant() {
        throw new IllegalAccessError("Constant Class");
    }
}
