package com.astralife.emp.constant;

public class DepartmentManagerConstant {

    public static final String DEPARTMENT_MANAGER_NOT_FOUND = "Department Manager not found";
    public DepartmentManagerConstant() {
        throw new IllegalAccessError("Constant Class");
    }
}
