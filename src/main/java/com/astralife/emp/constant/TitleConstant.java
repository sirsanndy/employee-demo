package com.astralife.emp.constant;

public class TitleConstant {
    public static final String TITLE_NOT_FOUND = "Title not found";

    public TitleConstant() {
        throw new IllegalAccessError("Constant Class");
    }
}
