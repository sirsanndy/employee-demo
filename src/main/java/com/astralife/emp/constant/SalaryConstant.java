package com.astralife.emp.constant;

public class SalaryConstant {
    public static final String SALARY_NOT_FOUND = "Salary not found";

    public SalaryConstant() {
        throw new IllegalAccessError("Constant Class");
    }
}
