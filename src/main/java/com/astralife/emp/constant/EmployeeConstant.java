package com.astralife.emp.constant;

public class EmployeeConstant {
    public static final String EMPLOYEE_NOT_FOUND = "Employee not found";

    public EmployeeConstant() {
        throw new IllegalAccessError("Constant Class");
    }
}
