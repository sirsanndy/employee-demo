package com.astralife.emp.constant;

public class DepartmentEmployeeConstant {
    public static final String DEPARTMENT_EMPLOYEE_NOT_FOUND = "Department Employee not found";

    public DepartmentEmployeeConstant() {
        throw new IllegalAccessError("Constant Class");
    }
}
