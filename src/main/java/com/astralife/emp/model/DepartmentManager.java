package com.astralife.emp.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "dept_manager")
@IdClass(DepartmentManagerId.class)
public class DepartmentManager implements Serializable {

    private static final long serialVersionUID = 5320235839680784434L;

    @Id
    @NotNull(message = "Department No can't be null")
    @Column(name = "dept_no", nullable = false)
    private Long deptNo;

    @Id
    @NotNull(message = "Employee No can't be null")
    @Column(name = "emp_no", nullable = false)
    private Long empNo;

    @NotNull(message = "From Date can't be null")
    @Column(name = "from_date", nullable = false)
    private LocalDate fromDate;

    @NotNull(message = "To Date can't be null")
    @Column(name = "to_date")
    private LocalDate toDate;

    @ManyToOne
    @MapsId
    @JoinColumn(name = "emp_no")
    private Employee employee;

    @ManyToOne
    @MapsId
    @JoinColumn(name = "dept_no")
    private Department department;

}
