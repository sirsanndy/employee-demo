package com.astralife.emp.model;

import com.astralife.emp.enumeration.Gender;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "employees")
public class Employee implements Serializable {

    private static final long serialVersionUID = 4248922182052193427L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "emp_no", nullable = false)
    private Long empNo;

    @NotNull(message = "Birth Date can't be blank")
    @Column(name = "birth_date", nullable = false)
    private LocalDate birthDate;

    @NotBlank(message = "First Name can't be blank")
    @Column(name = "first_name", nullable = false)
    private String firstName;

    @NotBlank(message = "Last Name can't be blank")
    @Column(name = "Last_name", nullable = false)
    private String lastName;

    @NotNull(message = "Gender can't be null")
    @Column(length = 3, columnDefinition = "ENUM('F','M') default 'M'", nullable = false)
    @Enumerated(value = EnumType.STRING)
    private Gender gender;

    @NotNull(message = "Hire Date can't be blank")
    @Column(name = "hire_date", nullable = false)
    private LocalDate hireDate;

    @OneToMany(mappedBy = "empNo", cascade = CascadeType.REMOVE)
    @PrimaryKeyJoinColumn
    private List<DepartmentManager> departmentManagers;

    @OneToMany(mappedBy = "empNo", cascade = CascadeType.REMOVE)
    @PrimaryKeyJoinColumn
    private List<DepartmentEmployee> departmentEmployees;

    @OneToMany(mappedBy = "empNo", cascade = CascadeType.REMOVE)
    @PrimaryKeyJoinColumn
    private List<Salary> salaries;

    @OneToMany(mappedBy = "empNo", cascade = CascadeType.REMOVE)
    @PrimaryKeyJoinColumn
    private List<Title> titles;

    public String toString(){
        return this.getBirthDate().toString()+this.getFirstName()+this.getLastName()
                +this.getGender().toString()+this.getHireDate().toString();
    }
}
