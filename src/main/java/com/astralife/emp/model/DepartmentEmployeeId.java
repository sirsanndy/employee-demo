package com.astralife.emp.model;

import java.io.Serializable;

public class DepartmentEmployeeId implements Serializable {

    private static final long serialVersionUID = -2458738833275506870L;
    private Long empNo;
    private Long deptNo;
}
