package com.astralife.emp.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "titles")
@IdClass(TitleId.class)
public class Title implements Serializable {

    private static final long serialVersionUID = -4236417486483722893L;

    @Id
    @NotNull(message = "Employee No can't be null")
    @Column(name = "emp_no", nullable = false)
    private Long empNo;

    @Id
    @NotEmpty(message = "Title can't be null")
    @Column(name = "title")
    private String title;

    @Id
    @NotNull(message = "From Date can't be null")
    @Column(name = "from_date")
    private LocalDate fromDate;

    @Column(name = "to_date")
    private LocalDate toDate;

    @ManyToOne
    @MapsId
    @JoinColumn(name = "emp_no")
    private Employee employee;
}
