package com.astralife.emp.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.time.LocalDate;

public class TitleId implements Serializable {

    private static final long serialVersionUID = -1032015735052776378L;
    private Long empNo;
    private String title;
    private LocalDate fromDate;
}
