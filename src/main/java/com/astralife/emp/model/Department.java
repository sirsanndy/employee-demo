package com.astralife.emp.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "departments")
public class Department implements Serializable {

    private static final long serialVersionUID = -8332100276414998372L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "dept_no", unique = true, nullable = false, length = 5)
    private Long deptNo;

    @NotBlank(message = "Dept Name can't be blank")
    @Column(name = "dept_name", nullable = false)
    private String deptName;

    @OneToMany(mappedBy = "deptNo", cascade = CascadeType.REMOVE)
    @PrimaryKeyJoinColumn
    private List<DepartmentManager> departmentManagers;

    @OneToMany(mappedBy = "deptNo", cascade = CascadeType.REMOVE)
    @PrimaryKeyJoinColumn
    private List<DepartmentEmployee> departmentEmployees;

    public String toString(){
        return this.getDeptName();
    }
}
