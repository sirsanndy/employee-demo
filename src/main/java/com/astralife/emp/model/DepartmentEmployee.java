package com.astralife.emp.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "dept_emp")
@IdClass(DepartmentEmployeeId.class)
public class DepartmentEmployee implements Serializable {

    private static final long serialVersionUID = 6987838414350017441L;

    @Id
    @NotNull(message = "Employee No can't be null")
    @Column(name = "emp_no", nullable = false)
    private Long empNo;

    @Id
    @NotNull(message = "Department No can't be null")
    @Column(name = "dept_no", nullable = false)
    private Long deptNo;

    @NotNull(message = "From Date can't be null")
    @Column(name = "from_date", nullable = false)
    private LocalDate fromDate;

    @NotNull(message = "To Date can't be null")
    @Column(name = "to_date")
    private LocalDate toDate;

    @ManyToOne
    @MapsId
    @JoinColumn(name = "emp_no")
    private Employee employee;

    @ManyToOne
    @MapsId
    @JoinColumn(name = "dept_no")
    private Department department;

}
