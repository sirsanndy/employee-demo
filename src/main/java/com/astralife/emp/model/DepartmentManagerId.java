package com.astralife.emp.model;

import java.io.Serializable;

public class DepartmentManagerId implements Serializable {

    private static final long serialVersionUID = -1234985007452821508L;
    private Long deptNo;
    private Long empNo;
}
