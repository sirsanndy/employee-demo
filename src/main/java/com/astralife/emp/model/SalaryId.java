package com.astralife.emp.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.time.LocalDate;

public class SalaryId implements Serializable {

    private static final long serialVersionUID = 4263778189520335945L;

    private Long empNo;
    private LocalDate fromDate;
}
