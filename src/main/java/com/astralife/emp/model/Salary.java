package com.astralife.emp.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "salaries")
@IdClass(SalaryId.class)
public class Salary implements Serializable {

    private static final long serialVersionUID = -8211991309379331131L;

    @Id
    @NotNull(message = "Employee No can't be null")
    @Column(name = "emp_no", nullable = false)
    private Long empNo;

    @NotNull(message = "Salary can't be null")
    @Column(name = "salary")
    private Long salary;

    @Id
    @NotNull(message = "From Date can't be null")
    @Column(name = "from_date")
    private LocalDate fromDate;

    @NotNull(message = "To Date can't be null")
    @Column(name = "to_date")
    private LocalDate toDate;

    @ManyToOne
    @MapsId
    @JoinColumn(name = "emp_no")
    private Employee employee;
}
