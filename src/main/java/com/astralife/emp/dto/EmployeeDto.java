package com.astralife.emp.dto;

import com.astralife.emp.enumeration.Gender;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class EmployeeDto {
    @JsonFormat(pattern="yyyy-MM-dd")
    private LocalDate birthDate;
    private String firstName;
    private String lastName;
    private Gender gender;

    @JsonFormat(pattern="yyyy-MM-dd")
    private LocalDate hireDate;
}
