package com.astralife.emp.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class SalaryDto {
    private Long empNo;
    private Long salary;

    @JsonFormat(pattern="yyyy-MM-dd")
    private LocalDate fromDate;

    @JsonFormat(pattern="yyyy-MM-dd")
    private LocalDate toDate;
}
