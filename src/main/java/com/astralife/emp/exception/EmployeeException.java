package com.astralife.emp.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EmployeeException extends RuntimeException {
    private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeException.class);
    private static final long serialVersionUID = -2985786318511150794L;

    public EmployeeException(String message) {
        super(message);
        LOGGER.error(message);
    }

    public EmployeeException(String message, Throwable e) {
        super(message, e);
        LOGGER.error(message, e);
    }

    public EmployeeException() {

    }
}