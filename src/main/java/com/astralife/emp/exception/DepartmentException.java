package com.astralife.emp.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DepartmentException extends RuntimeException{
    private static final Logger LOGGER = LoggerFactory.getLogger(DepartmentException.class);
    private static final long serialVersionUID = -3360881775123523737L;

    public DepartmentException(String message) {
        super(message);
        LOGGER.error(message);
    }

    public DepartmentException(String message, Throwable e) {
        super(message, e);
        LOGGER.error(message, e);
    }
}
