package com.astralife.emp.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DepartmentManagerException extends RuntimeException{
    private static final Logger LOGGER = LoggerFactory.getLogger(DepartmentManagerException.class);
    private static final long serialVersionUID = 5448859958060604633L;

    public DepartmentManagerException(String message) {
        super(message);
        LOGGER.error(message);
    }

    public DepartmentManagerException(String message, Throwable e) {
        super(message, e);
        LOGGER.error(message, e);
    }
}
