package com.astralife.emp.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SalaryException extends RuntimeException{
    private static final Logger LOGGER = LoggerFactory.getLogger(SalaryException.class);
    private static final long serialVersionUID = -6576590566775666210L;

    public SalaryException(String message) {
        super(message);
        LOGGER.error(message);
    }

    public SalaryException(String message, Throwable e) {
        super(message, e);
        LOGGER.error(message, e);
    }
}
