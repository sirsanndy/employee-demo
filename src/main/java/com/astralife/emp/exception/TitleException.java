package com.astralife.emp.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class TitleException extends RuntimeException {
    private static final Logger LOGGER = LoggerFactory.getLogger(TitleException.class);
    private static final long serialVersionUID = 160781710616409562L;

    public TitleException(String message) {
        super(message);
        LOGGER.error(message);
    }

    public TitleException(String message, Throwable e) {
        super(message, e);
        LOGGER.error(message, e);
    }

    public TitleException() {

    }
}
