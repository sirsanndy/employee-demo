package com.astralife.emp.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DepartmentEmployeeException extends RuntimeException {

    private static final Logger LOGGER = LoggerFactory.getLogger(DepartmentEmployeeException.class);
    private static final long serialVersionUID = 7970380726016872733L;

    public DepartmentEmployeeException(String message) {
        super(message);
        LOGGER.error(message);
    }

    public DepartmentEmployeeException(String message, Throwable e) {
        super(message, e);
        LOGGER.error(message, e);
    }
}
